Frost line - Wikipedia, the free encyclopedia
Frost line
For other uses, see Frost line (disambiguation).
Not to be confused with the geographical feature Snow line.
The frost line—also known as frost depth or freezing depth—is most commonly the depth to which the groundwater in soil is expected to freeze. The frost depth depends on the climatic conditions of an area, the heat transfer properties of the soil and adjacent materials, and on nearby heat sources. For example, snow cover and asphalt insulate the ground and homes can heat the ground (see also heat island). The line varies by latitude, it is deeper closer to the poles. It ranges in the United States from about zero to six feet. Below that depth, the temperature varies, but is always above 0 °C (32 °F).
Alternatively, in Arctic and Antarctic locations the freezing depth is so deep that it becomes year-round permafrost, and the term "thaw depth" is used instead. Finally, in tropical regions, frost line may refer to the vertical geographic elevation below which frost does not occur.[1]
Frost front refers to the varying position of the frost line during seasonal periods of freezing and thawing.
Building codes[edit]
Building codes sometimes take frost depth into account because of frost heaving which can damage buildings by moving their foundations. Foundations are normally built below the frost depth for this reason. Water pipes are normally buried below the frost line, or insulated to prevent them from freezing.
There are many ways to predict frost depth including factors which relate air temperature to soil temperature.
Sample frost lines for various locations[edit]
USA
Minnesota (2007):
Northern counties: 5 feet (1.5 m)[2][3]
Southern counties: 3.5 feet (1.1 m)[2][3]
Canada: Can use table of estimates based on freezing index degree days.[4]
Ontario: Map of frost penetration depths for Southern Ontario.[5]
Ottawa: 1.8 metres (5.9 ft) [5][6]
Windsor: 1.0 metre (3.3 ft) [5][6]
References[edit]
^ "Frost line". Dictionary.com. Retrieved 2008-06-11.
^ a b "Frost Depth: Minnesota State Building Code Rules 1303.1600" (PDF) (2007 ed.). Minnesota Department of Labor and Industry. 2007-03-09. Retrieved 2010-01-04.
^ a b "The 2007 Minnesota State Building Code" (PDF). Minnesota Department of Labor and Industry. 2007-10-09. Retrieved 2010-01-04.
^ "Ambient Temperatures - Below Ground". Urecon. Retrieved 16 January 2014.
^ a b c "Foundation, Frost Penetration Depths for Southern Ontario (OPSD 3090.101 rev.1)" (PDF). Ontario Ministry of Transportation. Nov 2010. Retrieved 16 January 2014.
^ a b "Frost Heave (Civil Engineering vol.1 No.11)". Walters Forensic Engineering. Retrieved 16 January 2014.
Retrieved from "https://en.wikipedia.org/w/index.php?title=Frost_line&oldid=711275148"
Categories: SoilGlaciologyGround freezingFoundations (buildings and structures)