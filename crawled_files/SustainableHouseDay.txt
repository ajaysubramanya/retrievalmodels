Sustainable House Day - Wikipedia, the free encyclopedia
Sustainable House Day
Sustainable House Day is a national event held annually across Australia, where households open their doors to the public to give interested people the chance to learn about the benefits of energy efficiency in their homes.[1] People have the opportunity to talk with sustainable home owners, architects, builders, designers and energy auditors to learn about sustainable living.
References[edit]
^ http://www.weeklytimesnow.com.au/article/2009/07/07/89755_machine.html
External links[edit]
Sustainable House Day (official site)
Retrieved from "https://en.wikipedia.org/w/index.php?title=Sustainable_House_Day&oldid=687335393"
Categories: Environmental awareness daysAnnual events in AustraliaSeptember observances