World Energy Engineering Congress - Wikipedia, the free encyclopedia
World Energy Engineering Congress
The World Energy Engineering Congress (WEEC) is an international energy industry conference and exposition hosted annually by the Association of Energy Engineers.[1]
Professionals in the field of energy engineering from around the world convene annually at the WEEC to discuss energy-related issues and technology such as:
Energy efficiency and energy management
Renewable, green, and alternative energy
Combined heat and power, cogeneration, and distributed generation
Integrated building automation and energy management
Lighting efficiency
HVAC systems and controls
Thermal storage and load management
Boilers and combustion controls
Geoexchange technologies
Solar and fuel cell technologies
Applications specific to federal emergency management programs
Energy services and project financing
References[edit]
^ "Association of Energy Engineers". Archived from the original on January 12, 2010. Retrieved 2010-10-21.
External links[edit]
Official website
energy.gov
aeecenter.org
Retrieved from "https://en.wikipedia.org/w/index.php?title=World_Energy_Engineering_Congress&oldid=702676899"
Categories: Engineering organizations