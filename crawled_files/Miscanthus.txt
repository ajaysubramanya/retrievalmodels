Miscanthus - Wikipedia, the free encyclopedia
Miscanthus
Miscanthus, silvergrass,[4] is a genus of African, Eurasian, and Pacific Island plants in the grass family.[5][6]
Species[3][7]
Miscanthus changii Y.N.Lee - Korea
Miscanthus depauperatus Merr. - Philippines
Miscanthus ecklonii (Nees) Mabb. - southern Africa
Miscanthus floridulus - China, Japan, Southeast Asia, Pacific Islands
Miscanthus fuscus (Roxb.) Benth . - Indian Subcontinent, Indochina, Pen Malaysia
Miscanthus junceus - southern Africa
Miscanthus lutarioriparius L.Liu ex S.L.Chen & Renvoize - Hubei, Hunan
Miscanthus nepalensis (Trin.) Hack. - Indian Subcontinent, Tibet, Yunnan, Myanmar, Vietnam, Pen Malaysia
Miscanthus nudipes (Griseb.) Hack. - Assam, Bhutan, Nepal, Sikkim, Tibet, Yunnan
Miscanthus × ogiformis Honda - Korea, Japan
Miscanthus oligostachyus Stapf. - Korea, Japan
Miscanthus paniculatus (B.S.Sun) S.L.Chen & Renvoize - Guizhou, Sichuan, Yunnan
Miscanthus sacchariflorus - Korea, Japan, northeastern China, Russian Far East
Miscanthus sinensis - Korea, Japan, China, Southeast Asia, Russian Far East; naturalized in New Zealand, North + South America
Miscanthus tinctorius (Steud.) Hack. - Japan
Miscanthus villosus Y.C.Liu & H.Peng - Yunnan
Miscanthus violaceus (K.Schum.) Pilg. - tropical Africa
formerly included[3]
see Chloris Pseudopogonatherum Saccharum Spodiopogon
Miscanthus affinis - Pseudopogonatherum quadrinerve
Miscanthus cotulifer - Spodiopogon cotulifer
Miscanthus polydactylos - Chloris elata
Miscanthus rufipilus - Saccharum rufipilum
Miscanthus tanakae - Pseudopogonatherum speciosum
Contents
1 Uses
1.1 M. giganteus
1.2 M. sinensis
2 References
3 External links
Uses[edit]
M. giganteus[edit]
Main article: Miscanthus giganteus
The sterile hybrid between M. sinensis and M. sacchariflorus, Miscanthus giganteus, has been trialed as a biofuel in Europe since the early 1980s. It can grow to heights of more than 3.5 m in one growth season. Its dry weight annual yield can reach 25 tonnes per hectare (10 tonnes per acre).[8] It is sometimes called "elephant grass", so is thus confused with the African grass Pennisetum purpureum, also called that.
The rapid growth, low mineral content, and high biomass yield of Miscanthus make it a favorite choice as a biofuel.[9] Miscanthus can be used as input for ethanol production, often outperforming corn and other alternatives in terms of biomass and gallons of ethanol produced. Additionally, after harvest, it can be burned to produce heat and steam for power turbines. In addition to the amount of CO2 emissions from burning the crop, any fossil fuels that might have been used in planting, fertilizing, harvesting, and processing the crop, as well as in transporting the biofuel to the point of use, must also be considered when evaluating its carbon load. Its advantage, though, is that it is not usually consumed by humans, making it a more available crop for ethanol and biofuel, than, say, corn and sugarcane. When mixed 50%-50% with coal, Miscanthus biomass can be used in some current coal-burning power plants without modifications.
M. sinensis[edit]
Winter miscanthus, an ornamental grass, growing in Southern Ontario, Canada
Main article: Miscanthus sinensis
M. sinensis is cultivated as an ornamental plant. In Japan, where it is known as susuki (すすき), it is considered an iconic plant of late summer and early autumn. It is mentioned in Man'yōshū (VIII:1538) as one of the seven autumn flowers (aki no nana kusa, 秋の七草). It is used for the eighth month in hanafuda playing cards. It is decorated with bush clover for the Mid-Autumn Festival. Miscanthus has also excellent fiber properties for papermaking.
References[edit]
^ lectotype designated by Coville, Contr. U.S. Natl. Herb. 9: 400 (8 Apr 1905)
^ Tropicos, Miscanthus Andersson
^ a b c Kew World Checklist of Selected Plant Families
^ "Miscanthus". Natural Resources Conservation Service PLANTS Database. USDA. Retrieved 13 July 2015.
^ Andersson, Nils Johan. 1855. Öfversigt af Förhandlingar: Kongl. Svenska Vetenskaps-Akademien 12: 165.
^ Flora of China Vol. 22 Page 581 芒属 mang shu Miscanthus Andersson, Öfvers. Kongl. Vetensk.-Akad. Förh. 12: 165. 1855.
^ The Plant List search for Miscanthus
^ National Non-Food Crops Centre. "NNFCC Crop Factsheet: Miscanthus". Retrieved on 2011-02-17.
^ Scurlock, J. M. O. (February 1999). "Miscanthus: a review of European experience with a novel energy crop". Oak Ridge National Laboratory, U.S. Department of Energy. Retrieved 2009-06-01.
External links[edit]
UK's National Centre for Biorenewable Energy, Fuels and Materials
Miscanthus x giganteus - as an energy crop - Miscanthus Research at the University of Illinois
[1] - New Energy Farms - Miscanthus developers and suppliers
[2] - Terravesta - The complete supply chain solution for Miscanthus Europe.
Retrieved from "https://en.wikipedia.org/w/index.php?title=Miscanthus&oldid=671207151"
Categories: Poaceae generaEnergy cropsPanicoideaeHidden categories: Articles with 'species' microformats