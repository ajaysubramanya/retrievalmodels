Philosophical Transactions of the Royal Society A - Wikipedia, the free encyclopedia
Philosophical Transactions of the Royal Society A
Philosophical Transactions of the Royal Society A: Mathematical, Physical and Engineering Sciences is a fortnightly peer-reviewed scientific journal published by the Royal Society. It publishes original research and review content in a wide range of physical scientific disciplines. Articles can be accessed online a few months prior to the printed journal. All articles become freely accessible two years after their publication date. The current editor-in-chief is Dave Garner (University of Nottingham).
Contents
1 Overview
2 History
3 Impact
4 References
5 External links
Overview[edit]
Philosophical Transactions of the Royal Society A publishes themed journal issues on topics of current scientific importance and general interest within the physical, mathematical and engineering sciences, edited by leading authorities and comprising original research, reviews and opinions from prominent researchers. Past issue titles include "Supercritical fluids - green solvents for green chemistry?", "Tsunamis: Bridging science, engineering and society", "Spatial transformations: from fundamentals to applications", and "Before, behind and beyond the discovery of the Higgs boson".
History[edit]
Philosophical Transactions of the Royal Society was established in 1665 by the Royal Society and is the oldest scientific journal in the English-speaking world.[1] Henry Oldenburg was appointed as the first (joint) secretary to the society and he was also the first editor of the society's journal. In 1887 the journal expanded to become two separate publications, one serving the physical sciences, Philosophical Transactions of the Royal Society A: Physical, Mathematical and Engineering Sciences, and the other focusing on the life sciences, Philosophical Transactions of the Royal Society B: Biological Sciences. Nowadays, both journals publish themed issues and discussion meeting issues, while individual research articles are published in the sister journal Proceedings of the Royal Society.
The journal celebrated its 350th anniversary in 2015. To commemorate this event it published a special collection[2] of commentaries on landmark papers from the archive by scientists such as Isaac Newton, Humphry Davy and Michael Faraday.
Impact[edit]
According to the Journal Citation Reports, the journal has a 2014 impact factor of 2.147, ranking it 11th out of 56 journals in the category "Multidisciplinary Sciences".[3]
References[edit]
^ Philosophical Transactions: 350 years of publishing at the Royal Society (1665 – 2015) (PDF). The Royal Society.
^ "Celebrating 350 years of Philosophical Transactions: physical sciences papers". Phil. Trans. R. Soc. A 373 (2039). 6 March 2015.
^ "Journals Ranked by Impact: Multidisciplinary Sciences". 2014 Journal Citation Reports. Web of Science (Science ed.). Thomson Reuters. 2015.  |access-date= requires |url= (help)
External links[edit]
Official website
Retrieved from "https://en.wikipedia.org/w/index.php?title=Philosophical_Transactions_of_the_Royal_Society_A&oldid=710672380"
Categories: Multidisciplinary scientific journalsPublications established in 1887Biweekly journalsEnglish-language journalsRoyal Society academic journals1887 establishments in the United KingdomHidden categories: Pages using citations with accessdate and no URL