Solar combisystem - Wikipedia, the free encyclopedia
Solar combisystem
(Redirected from Geosolar)
A solar combisystem provides both solar space heating and cooling as well as hot water from a common array of solar thermal collectors, usually backed up by an auxiliary non-solar heat source.
Solar combisystems may range in size from those installed in individual properties to those serving several in a block heating scheme. Those serving larger groups of properties district heating tend to be called central solar heating schemes.
A large number of different types of solar combisystems are produced - over 20 were identified in the first international survey, conducted as part of IEA SHC Task 14 [1] in 1997. The systems on the market in a particular country may be more restricted, however, as different systems have tended to evolve in different countries. Prior to the 1990s such systems tended to be custom-built for each property. Since then commercialised packages have developed and are now generally used.
Depending on the size of the combisystem installed, the annual space heating contribution can range from 10% to 60% or more in ultra-low energy Passivhaus type buildings; even up to 100% where a large interseasonal thermal store or concentrating solar thermal heat is used. The remaining heat requirement is supplied by one or more auxiliary sources in order to maintain the heat supply once the solar heated water is exhausted. Such auxiliary heat sources may also use other renewable energy sources (when a geothermal heat pump is used, the combisystem is called geosolar)[2] and, sometimes, rechargeable batteries.
During 2001, around 50% of all the domestic solar collectors installed in Austria, Switzerland, Denmark, and Norway were to supply combisystems, while in Sweden it was greater. In Germany, where the total collector area installed (900,000 m2) was much larger than in the other countries, 25% was for combisystem installations. Combisystems have also been installed in Canada since the mid-1980s.
Some combisystems can incorporate solar thermal cooling in summer.[3]
Contents
1 Classification
2 Combisystem design
3 Technologies
4 Relationship to low energy building
5 See also
6 External links
7 References
7.1 Footnotes
Classification[edit]
Following the work of IEA SHC Task 26 (1998 to 2002), solar combisystems can be classified according to two main aspects; firstly by the heat (or cool) storage category (the way in which water is added to and drawn from the storage tank and its effect on stratification); secondly by the auxiliary heat (or cool) management category (the way in which non-solar-thermal auxiliary heaters or coolers can be integrated into the system).
Maintaining stratification (the variation in water temperature from cooler at the foot of a tank to warmer at the top) is important so that the combisystem can supply hot or cool water and space heating and cooling water at different temperatures.
A solar combisystem may therefore be described as being of type B/DS, CS, etc.
Within these types, systems may be configured in many different ways. For the individual house they may – or may not – have the storage tanks, controls and auxiliary heater and cooler integrated into a single prefabricated package. In contrast, there are also large centralised systems serving a number of properties.
The simplest combisystems – the Type A – have no "controlled storage device". Instead they pump warm (or cool) water from the solar collectors through underfloor central heating pipes embedded in the concrete floor slab. The floor slab is thickened to provide thermal mass and so that the heat and cool from the pipes (at the bottom of the slab) is released during the evening.
Combisystem design[edit]
The size and complexity of combisystems, and the number of options available, mean that comparing design alternatives is not straightforward. Useful approximations of performance can be produced relatively easily, however accurate predictions remain difficult.
Tools for designing solar combisystems are available, varying from manufacturer's guidelines to nomograms (such as the one developed for IEA SHC Task 26) to various computer simulation software of varying complexity and accuracy.
Among the software and packages are CombiSun (released free by the Task 26 team,[4] which can be used for basic system sizing) and the free SHWwin (Austria, in German [5]). Other commercial systems are available.
Solar combisystems generally use underfloor heating and cooling [1].
Concentrating solar thermal technology may be used to make the collectors as small as possible.
Technologies[edit]
Solar combisystems use similar technologies to those used for solar hot water and for regular central heating and underfloor heating, as well as those used in the auxiliary systems - microgeneration technologies or otherwise.
The element unique to combisystems is the way that these technologies are combined, and the control systems used to integrate them, plus any stratifier technology that might be employed.
Relationship to low energy building[edit]
By the end of the 20th century solar hot water systems had been capable of meeting a significant portion of domestic hot water requirements in many climate zones. However it was only with the development of reliable low-energy building techniques in the last decades of the century that extending such systems for space heating became realistic in temperate and colder climatic zones.
As heat demand reduces, the overall size and cost of the system is reduced, and the lower water temperatures typical of solar heating may be more readily used - especially when coupled with underfloor heating or wall heating. The volume occupied by the equipment also reduces, which also increases the flexibility of its location.
In common with other heating systems in low-energy buildings, system performance is more sensitive to the number of occupants, room temperature and ventilation rates, when compared to regular buildings where such effects are small in relation to the higher overall energy demand.
See also[edit]
Geothermal heat pump
Renewable heat concentrating solar (used in theses systems to produce heating and not to make electricity)
Renewable energy
Solar cooling
Solar heating
Central solar heating
Solar thermal energy
External links[edit]
IEA SHC Task 26 official site
The European Altener Programme Project: Solar Combisystems
Test of Thermal Solar Systems for Hot Water and Space Heating (June 2004)
Interseasonal Heat Transfer integrates solar thermal collection and thermal storage
Combisystem test reports - Mostly in German
References[edit]
Solar Heating Systems for Houses – A Design Handbook for Solar Combisystems, James and James, ISBN 1-902916-46-8 (by the Task 26 team)
Footnotes[edit]
^ http://www.iea-shc.org/task14/index.html
^ http://www.sofath.com/
^ http://www.itw.uni-stuttgart.de/ITWHomepage/Forschung/Folie.pdf
^ http://www.elle-kilde.dk/altener-combi/dwload.html
^ http://www.iwt.tugraz.at/downloads.htm
Retrieved from "https://en.wikipedia.org/w/index.php?title=Solar_combisystem&oldid=685916385"
Categories: Geothermal energySolar powerSolar thermal energyHeating, ventilating, and air conditioningResidential heating