Wind power in Germany - Wikipedia, the free encyclopedia
Wind power in Germany
Wind farm in Neuenkirchen
Erection of an Enercon E70-4 in Germany
Wind power in Germany describes wind power in Germany as part of energy in Germany and renewable energy in Germany. In 2011, the installed capacity of wind power in Germany was 29,075 megawatts (MW), with wind power producing about 7.7 percent of Germany’s total electrical power.[1] According to EWEA in a normal wind year, installed wind capacity in Germany will meet 10.6% at end 2011 and 9.3% at end 2010 of the German electricity needs.[2][3] The persistent disparity between EWEA estimates for a "normal wind year" and the actual data tabulated below is due to the EWEA relying on an unrealistically high capacity factor for German wind production.[citation needed]
More than 21,607 wind turbines are located in the German federal area and the country has plans to build more wind turbines.[4][5] As of 2011, Germany's federal government is working on a new plan for increasing renewable energy commercialization,[6] with a particular focus on offshore wind farms.[7] A major challenge will be the development of sufficient network capacities for transmitting the power generated in the North Sea to the large industrial consumers in southern Germany.[8]
Contents
1 Overview
2 Repowering
3 Offshore wind power
4 Energy transition
5 Statistics
5.1 States
6 See also
7 References
8 External links
Overview[edit]
As of 2010, Wind power in Germany provides over 96,100 people with jobs and German wind energy systems are also exported.[4][9] The Fuhrländer Wind Turbine Laasow, built in 2006 near the village of Laasow, Brandenburg, was for six years the tallest wind turbine in the world.
In Germany, hundreds of thousands of people have invested in citizens' wind farms across the country and thousands of small and medium-sized enterprises are running successful businesses in a new sector that in 2008 employed 90,000 people and generated 8 percent of Germany's electricity.[10] Wind power has gained very high social acceptance in Germany.[11]
Repowering[edit]
Repowering, the replacement of first-generation wind turbines with modern multi-megawatt machines, is occurring in Germany. Modern turbines make better use of available wind energy and so more wind power can come from the same area of land. Modern turbines also offer much better grid integration since they use a connection method similar to conventional power plants.[12][13]
Offshore wind power[edit]
Offshore wind farms in the German Bight
See also: List of offshore wind farms in Germany, Alpha Ventus Offshore Wind Farm, Baltic 1 Offshore Wind Farm, BARD Offshore 1 and Borkum Riffgat
Offshore wind energy also has great potential in Germany.[14] Wind speed at sea is 70 to 100% higher than onshore and much more constant. A new generation of 5 MW or larger wind turbines which are capable of making full use of the potential of wind power at sea has already been developed and prototypes are available. This makes it possible to operate offshore wind farms in a cost-effective way once the usual initial difficulties of new technologies have been overcome.[15]
On 15 July 2009, the first offshore German windturbine completed construction. This turbine is the first of a total of 12 wind turbines for the alpha ventus offshore wind farm in the North Sea.[16]
Following the 2011 Japanese nuclear accidents, Germany's federal government is working on a new plan for increasing renewable energy commercialization, with a particular focus on offshore wind farms.[17] Under the plan, large wind turbines will be erected far away from the coastlines, where the wind blows more consistently than it does on land, and where the enormous turbines won't bother the inhabitants. The plan aims to decrease Germany's dependence on energy derived from coal and nuclear power plants.[7] The German government wants to see 7.6 GW installed by 2020 and as much as 26 GW by 2030.[18]
A major challenge will be the lack of sufficient network capacities for transmitting the power generated in the North Sea to the large industrial consumers in southern Germany.[8]
In 2014, all in all 410 turbines with 1747 megawatts were added to Germany's offshore windparks. Due to not yet finished grid-connections, only turbines with combined 528.9 megawatts were added to the grid feed at the end of 2014. Despite this, the gigawatt offshore windpower barrier has been breached by Germany at the end of 2014.[19]
Energy transition[edit]
Main article: Energy Transition in Germany
The 2010 "Energiewende" policy has been embraced by the German federal government and has resulted in a huge expansion of renewables, particularly wind power. Germany's share of renewables has increased from around 5% in 1999 to 17% in 2010, reaching close to the OECD average of 18% usage of renewables.[20] Producers have been guaranteed a fixed feed-in tariff for 20 years, guaranteeing a fixed income. Energy co-operatives have been created, and efforts were made to decentralize control and profits. The large energy companies have a disproportionately small share of the renewables market. Nuclear power plants were closed, and the existing 9 plants will close earlier than necessary, in 2022.
The reduction of reliance on nuclear plants has so far had the consequence of increased reliance on fossil fuels and on electricity imports from France. However, in good wind Germany exports to France; in January 2015 the average price was €29/MWh in Germany, and €39/MWh in France.[21] One factor that has inhibited efficient employment of new renewable energy has been the lack of an accompanying investment in power infrastructure (SüdLink) to bring the power to market.[20][22] The transmission constraint sometimes causes Germany to pay Danish wind power to stop producing; in October/November 2015 this amounted to 96 GWh costing 1.8 million euros.[23]
Different Länder have varying attitudes to the construction of new power lines. Industry has had their rates frozen and so the increased costs of the Energiewende have been passed on to consumers, who have had rising electricity bills. Germans in 2013 had some of the highest electricity costs in Europe.[24]
Statistics[edit]
Annual Wind Power in Germany 1990-2014, shown in a semi-log plot with installed capacity (MW) in red and power generated (GWh) in blue; the close correlation shows a fairly consistent yearly-averaged capacity factor, of about ⅙.
Installed wind power capacity and generation in recent years is shown in the table below:[25]
Offshore only:
States[edit]
Geographic distribution of wind farms in Germany
In Saxony-Anhalt 48.11% of electricity was produced with wind power in 2011.[26]
Share of the potential annual energy yield of the net electrical energy consumption in 2011:
See also[edit]
Renewable energy in Germany
Solar power in Germany
Wind power in the European Union
References[edit]
^ Bundesministerium für Wirtschaft und Technologie (February 2012). "Die Energiewende in Deutschland" (PDF). Berlin. p. 4.
^ Wind in power 2011 European statistics EWEA February 2012, pages 4 and 11
^ Wind in power 2010 European statistics EWEA February 2011, page 11
^ a b "Wind energy in Germany".
^ "72,6 Gigawatts Worldwide" (PDF). Wind Energy Barometer. February 2007. Retrieved 4 July 2007.
^ "100% renewable electricity supply by 2050". Federal Ministry for Environment, Nature Conservation and Nuclear Safety. 26 January 2011. Retrieved 4 June 2011.
^ a b Schultz, Stefan (23 March 2011). "Will Nuke Phase-Out Make Offshore Farms Attractive?". Spiegel Online. Retrieved 26 March 2011.
^ a b The Wall Street Journal Online, 24 April 2012
^ "General Information - Wind Energy". Federal Ministry for Environment, Nature Conservation and Nuclear Safety. Retrieved 4 June 2011.
^ "Community Power Empowers". Dsc.discovery.com. 26 May 2009. Retrieved 17 January 2012.
^ Community Wind Farms at the Wayback Machine (archived July 20, 2008)
^ Hochstätter, Matthias; Paulsen, Thorsten; Grotz, Claudia (May 2006). "A clean issue -- Wind energy in germany" (PDF). BWE-Bundesverband Windenergie. p. 18.
^ Fairley, Peter (19 January 2009). "Europe Replaces Old Wind Farms". IEEE Spectrum. Retrieved 24 January 2009.
^ Rehfeldt, Dr. Knud (January 2007). "Offshore wind power deployment in Germany" (PDF). Federal Ministry for Environment, Nature Conservation and Nuclear Safety. Retrieved 4 June 2011.
^ Kuhbier, Jörg (22 February 2007). "Offshore Wind Power in Germany" (PDF). Federal Ministry for Environment, Nature Conservation and Nuclear Safety. Retrieved 21 June 2007.
^ Alpha Ventus
^ Dohmen, Frank; Jung, Alexander (27 April 2011). "Why Germany's Offshore Wind Parks Have Stalled". Spiegel Online. Retrieved 1 January 2012.
^ Dohmen, Frank; Jung, Alexander (30 December 2011). "Stress on the High Seas: Germany's Wind Power Revolution in the Doldrums". Spiegel Online. Retrieved 1 January 2012.
^ "Offshore Wind Energy in Germany 2014". Bundesverband WindEnergie e.V. Retrieved 30 January 2015.
^ a b "Germany’s energy transformation Energiewende". The Economist. Jul 28, 2012. Retrieved 6 March 2013.
^ Click Green (3 February 2015). "UK and German wind energy records drive down winter electricity bills". clickgreen.org.uk.
^ Knight, Sara (29 May 2015). "Politics block German offshore wind link". windpowermonthly.com. Archived from the original on 6 September 2015.
^ Jesper Starn, Weixin Zha (1 December 2015). "Germany Pays to Halt Danish Wind Power to Protect Own Output". Bloomberg News.
^ "Germany’s energy reform Troubled turn". The Economist. 9 Feb 2013. Retrieved 6 March 2013.
^ "Zeitreihen zur Entwicklung der erneuerbaren Energien in Deutschland". Federal Ministry for Economic Affairs and Energy (Germany). Dec 2015. Retrieved 27 Feb 2016.
^ "Status der Windenergienutzung in Deutschland 31.12.2011 DEWI" [Status of wind energy in Germany 31.12.2011 DEWI] (PDF) (in German). DEWI-Deutsches Windenergie-Institut. Retrieved 28 February 2012.
External links[edit]
Germany Inaugurates 5 MW Wind Turbine Prototype
5-MW BARD Near-shore Wind Turbine Erected in Germany
Deutsche Energie-Agentur (Dena), German Energy Agency
Official site about wind power and renewable Energy in the Emscher-Lippe-Region
Cost-optimal expansion of renewables would save Germany up to two billion euros a year
Retrieved from "https://en.wikipedia.org/w/index.php?title=Wind_power_in_Germany&oldid=707198752"
Categories: Wind power in GermanyHidden categories: CS1 German-language sources (de)All articles with unsourced statementsArticles with unsourced statements from January 2016Commons category with local link same as on Wikidata