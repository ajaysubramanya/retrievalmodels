Renewable energy in Asia - Wikipedia, the free encyclopedia
Renewable energy in Asia
Renewable energy is a viable means of generating energy in Asia.
For solar power, South Asia has the ideal combination of both high solar insolation [1] and a high density of potential customers.[2][3][4]
Cheap solar can bring electricity to a major chunk of subcontinent's people who still live off-grid, bypassing the need of installation of expensive grid lines. Also since the costs of energy consumed for temperature control squarely influences a regions energy intensity, and with cooling load requirements roughly in phase with the sun's intensity, cooling from intense solar radiation could make perfect energy-economic sense in the subcontinent.[5][6][7]
Contents
1 Renewable energy by country
1.1 Afghanistan
1.2 Bangladesh
1.3 China
1.4 India
1.5 Indonesia
1.6 Israel
1.7 Japan
1.8 Lebanon
1.9 Nepal
1.10 Pakistan
1.11 Philippines
1.12 South Korea
2 See also
3 References
4 External links
Renewable energy by country[edit]
Afghanistan[edit]
Main article: Renewable energy in Afghanistan
Bangladesh[edit]
Main article: Renewable energy in Bangladesh
In Bangladesh, a number of domestic solar energy systems are in use in houses around the country. The use of solar energy on this scale is highly potential and advantageous as more than 60% of areas in the country do not have access to main grid electricity. The World Bank is backing a program of making solar energy available to wider population in Bangladesh, as part of the Rural Electrification and Renewable Energy Development Project (REREDP), which subsidizes solar energy systems.
A typical 'solar home system' can power two to eight 'low energy' lights, plus a socket for TV, radio or battery recharging, and a mobile telephone charging unit, too. Each system consists of a solar photovoltaic panel, mounted on the house roof. Depending on its size, this provides between 40W and 135W of electricity in full sunlight (the most common being 50W).
Grameen Shakti is the largest organization installing rural based solar home system (SHS) in Bangladesh. Other companies working on similar solar energy based SHS are Rural Services Foundation (RSF), Brac, Hilfulfujal and so on. The model of micro finance based SHS is now being copied in other parts of the world as a successful business model.
Rahimafrooz is a major supplier of high quality solar batteries and other solar components for the program. Rahimafrooz Renewable Energy Ltd (RRE) has been the pioneer in installing solar powered centralized systems, water pumps for irrigation and pure drinking water, water heaters, street lights, and solar-powered telecom solutions to various organizations. They are working closely with pertinent government organizations in installing solar powered medical refrigerator that provides emergency live saving medicines in the off-grid rural areas.
A company named Digital Technology is doing research and development of solar PV products like solar billboard lighting, mini grid system for irrigation etc.
China[edit]
Main article: Renewable energy in China
Rooftop solar water heaters are ubiquitous in modern China
Wind farm in Xinjiang, China
In China there now are six factories producing at least 2 GW/year each of monocrystalline, poly-crystalline and non-crystalline Photovoltaic cells. These factories include the LDK Solar Co, Wuxi Suntech Solar Energy Co., Ltd., which produces approximately 50 MW/year of solar cells and photovoltaic modules; the Yunnan Semi-conductor Parts Plant, which manufactures approximately 2 MW/year of mono-crystalline cells; the Baoding Yingli Solar Energy Modules Plant, which manufactures approximately 6 MW/year of polycrystalline cells and modules; the Shanghai Jiaoda Guofei Solar Energy Battery Factory, which produces approximately 1 MW/year of modules; and the Shanghai PV Science and Technology Co., Ltd., which produces approximately 5 MW/year of modules.[8]
China has become a world leader in the manufacture of solar photovoltaic technology, with its six biggest solar companies having a combined value of over $15 billion. Around 820 megawatts of solar PV were produced in China in 2007, second only to Japan.[9] Suntech Power Holdings Co based in Jiangsu, is the world's third- biggest supplier of solar cells.[10]
There are some obstacles to the further development of the Chinese solar energy sector that China faces. These obstacles include the lack of a nationwide comprehensive photovoltaic (PV) plan, the lack of updated facilities and sufficient financial resources to support PV research at research institutes, the lack of sufficient facilities and resources at companies manufacturing PV products, the failure of companies to be able to produce high quality, reliable and low cost PV products and the relatively weak educational and training opportunities in China for PV science and technology.[11]
About 50 MW of installed solar capacity was added in 2008, more than double the 20 MW in 2007, but still a relatively small amount. According to some studies, the demand in China for new solar modules could be as high as 232 MW each year from now on until 2012. The government has announced plans to expand the installed capacity to 1,800 MW by 2020. If Chinese companies manage to develop low cost, reliable solar modules, then the sky is the limit for a country that is desperate to reduce its dependence on coal and oil imports as well as the pressure on its environment by using renewable energy.[12]
In 2009 centre to the PRC Government’s plans is the recently announced "Golden Sun" stimulus program. Under this program the Ministry of Finance will subsidize half of the total construction costs of an on-grid solar power plant, including transmission expenses. The Ministry of Finance will also pay subsidies of up to 70% to develop independent photovoltaic power generating systems in remote regions. The strong handed move by the Government is meant to encourage more solar projects to increase the current solar power capacity, which at 2008 stood at a paltry 40MW. As the Government targets to increase China’s solar power capacity up to 20GW by 2020,[13] this will provide significant opportunities for solar cell and module manufacturers. Many of the solar industry players therefore will expect for chances to be benefited from the government programs especially the solar cell manufacturers. With the hope of increase in local demand, some of the new developments have been going on with this region, like Anwell Technologies Limited, a Singapore listed company having its solar cell manufacturing plant in China, has produced its first thin film solar panel with its own developed production lines in September 2009.[14]
According to the speech given by the Chinese President Hu Jintao's at the UN climate summit held on September 22, 2009 in New York, China will intensify effort and adopt ambitious plans to plant enough forest to cover an area the size of Norway and use 15 percent of its energy from renewable sources within a decade.[15]
India[edit]
Main article: Renewable energy in India
Solar Resource Map of India
India is both densely populated and has high solar insolation, providing an ideal combination for solar power in India. Much of the country does not have an electrical grid, so one of the first applications of solar power has been for water pumping, to begin replacing India's four to five million diesel powered water pumps, each consuming about 3.5 kilowatts, and off-grid lighting. Some large projects have been proposed, and a 35,000 km² area of the Thar Desert has been set aside for solar power projects, sufficient to generate 700 to 2,100 gigawatts.
The Indian Solar Loan Programme, supported by the United Nations Environment Programme has won the prestigious Energy Globe World award for Sustainability for helping to establish a consumer financing program for solar home power systems. Over the span of three years more than 16,000 solar home systems have been financed through 2,000 bank branches, particularly in rural areas of South India where the electricity grid does not yet extend.[16][17]
Launched in 2003, the Indian Solar Loan Programme was a four-year partnership between UNEP, the UNEP Risoe Centre, and two of India's largest banks, the Canara Bank and Syndicate Bank.[17]
According to Development Counsellors International (DCI), a United States marketing company, India is the second best country, after China, for business investment. United Nations Environment Programme (UNEP) has reported that India has seen a 12% increase in investment in the renewable energy sector with an investment of $3.7 billion in 2008. The largest share was asset finance at $3.2 billion which grew by 25%. The clean renewable energy includes wind, solar, biomass and small-hydro projects. The major portion of investment has been made in wind energy sector. The investment in wind energy sector grew at 17% from $2.2 billion to $2.6 billion.[18]
Indonesia[edit]
Main article: Energy in Indonesia
Israel[edit]
Further information: Energy in Israel § Renewable energy
Japan[edit]
Main article: Energy in Japan
Japan currently produces about 10% of its electricity from renewable sources. The renewable share goal is 20% by 2020.[19]
Lebanon[edit]
Further information: Geothermal energy in Lebanon
Nepal[edit]
Main article: Renewable energy in Nepal
Pakistan[edit]
Main article: Renewable energy in Pakistan
Philippines[edit]
Main article: Renewable energy in the Philippines
Bangui Wind Farm in Ilocos Norte, Philippines
The Philippine government sees the growth of the renewable energy sector essential for national energy security. The Philippines' fossil fuel sector is unsustainable, being dependent on the import of nonrenewable fuel, including petroleum, but has significant potential in the renewable energy sector. Based on a report of an Australian consulting firm, International Energy Consultants, the Philippines has the highest electricity rate in Asia, followed by Japan. Transmitting power and transporting fuel throughout the Philippine archipelago is problematic due to very high cost.[20]
The Philippines could be considered a world leader in renewable energy, with 30 percent of its power generation being powered by the renewable energy sector. The Philippines is the world's second largest generator of geothermal energy and was the first Southeast Asian nation to invest in large-scale solar and wind technologies.[20]
South Korea[edit]
In 2008, South Korea came 4th in the list of installed PV capacity according to EPIA statistics as a result of the favorable feed-in tariff system with a cap of 500MW in 2008. According to Displaybank, the new “PV Market Creation Plan” announced in 2009 is expected to boost the Korean PV installment market to increase to 200MW by 2012.[21][22] The government further announced plans to increase more than double its financing for renewable R&D projects to 3.5 trillion won ($2.9/£1.9bn) by 2013. The government also plans to expand its system of tax breaks to cover new technologies in solar such as wind and thermal power, low-emission vehicles and rechargeable batteries etc.[23]
See also[edit]
American Council on Renewable Energy (ACORE)
International Renewable Energy Agency (IRENA)
List of energy storage projects
List of renewable energy topics by country
Renewable energy in the European Union
Renewable energy in Africa
Renewable energy in developing countries
Renewable energy commercialization
Renewable energy development
Renewables Directive
Renewable Energy Policy Network(REN21)
Smart villages in Asia
Solar for all
Solar Energy Industries Association (SEIA)
United Nations Environment Organization
References[edit]
^ Energy-Atlas Solar radiation
^ NASA population density map
^ Solar LEDs Brighten Rural India's Future
^ Solar plan for Indian computers
^ Solar Cooling German report
^ Paper presented at the International Conference on Solar Air Conditioning, Germany
^ Solar Cooling - Case Studies
^ Solar Energy Booming in China
^ China pioneers in renewable energy
^ China to Be World's Top Manufacturer of Green Energy Technology
^ China to train developing nations in solar technologies
^ China's New Focus on Solar | Renewable Energy News Article
^ "Spain no longer leading the way in PV solar energy". Renewable energy magazine. 2009-08-26.
^ "Anwell Produces its First Thin Film Solar Panel". Solarbuzz. 2009-09-07.
^ "UN climate summit puts China, India in spotlight". The Miami Herald. 22 Sep 2009.
^ Consumer financing program for solar home systems in southern India
^ a b UNEP wins Energy Globe award
^ "India’s solar mission: How it is harnessing unlimited energy". EcoSeed. 23 Sep 2009.
^ "After Fukushima, Japan beginning to see the light in solar energy". The Guardian. 19 June 2013. Retrieved 19 June 2013.
^ a b The Right Mix: The Philippines Achieving its Renewable Energy Goals, Manila Bulletin
^ "Korea PV market expected to reach 200MW by 2012". Global Solar Technology. October 8, 2009.
^ "Korean solar energy market indicates growth slowing down". Solar Plaza. September 11, 2009.
^ "South Korea more than doubles green R&D funding". Business Green. 13 Jul 2010.
External links[edit]
South Korea to Invest $193 Million to Develop Clean Energy
Alliance for Rural Electrification (non-for profit business association for the promotion of renewable energy in developing countries)
Energy Investment in Asia and the Pacific
Retrieved from "https://en.wikipedia.org/w/index.php?title=Renewable_energy_in_Asia&oldid=695722216"
Categories: Renewable energy in AsiaEnergy in Asia