Energy in Lebanon - Wikipedia, the free encyclopedia
Energy in Lebanon
Location
Energy in Lebanon describes energy and electricity production, consumption and import in Lebanon. Energy policy of Lebanon will describe the energy policy in the politics of Lebanon more in detail. Lebanon imports most of its energy.
Primary energy use in 2009 in Lebanon was 77 TWh and 18 TWh per million persons.[1] Primary energy use in Lebanon was 61 TWh and 15 TWh/million persons in 2008.[1]
Contents
1 Overview
2 Gas
3 Renewable energy
4 See also
5 References
Overview[edit]
Gas[edit]
The Arab Gas Pipeline is a natural gas pipeline exporting Egyptian natural gas to Jordan, Syria and Lebanon, with a separate line to Israel.
Renewable energy[edit]
Lebanese government intend to meet 12 percent of its total energy needs from renewable energy sources by 2020.[3]
Further information: Geothermal energy in Lebanon
See also[edit]
Trans-Arabian Pipeline
References[edit]
^ a b IEA Key energy statistics 2011 Page: Country specific indicator numbers from page 48 Cite error: Invalid <ref> tag; name "IEA2010" defined multiple times with different content (see the help page).
^ IEA Key World Energy Statistics 2011, 2010, 2009, 2006 IEA October, crude oil p.11, coal p. 13 gas p. 15
^ Daily Star artile on geothermal energy in Lebanon
Retrieved from "https://en.wikipedia.org/w/index.php?title=Energy_in_Lebanon&oldid=656890740"
Categories: Energy in LebanonHidden categories: Pages with reference errorsPages with duplicate reference namesCommons category with local link same as on Wikidata