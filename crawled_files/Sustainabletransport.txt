Sustainable transport - Wikipedia, the free encyclopedia
Sustainable transport
Sustainable transport refers to the broad subject of transport that is sustainable in the senses of social, environmental and climate impacts and the ability to, in the global scope, supply the source energy indefinitely. Components for evaluating sustainability include the particular vehicles used for road, water or air transport; the source of energy; and the infrastructure used to accommodate the transport (roads, railways, airways, waterways, canals and terminals). Another component for evaluation is pipelines for transporting liquid or gas materials. Transport operations and logistics as well as transit-oriented development are also involved in evaluation. Transportation sustainability is largely being measured by transportation system effectiveness and efficiency as well as the environmental and climate impacts of the system.[1]
Short-term activity often promotes incremental improvement in fuel efficiency and vehicle emissions controls while long-term goals include migrating transportation from fossil-based energy to other alternatives such as renewable energy and use of other renewable resources. The entire life cycle of transport systems is subject to sustainability measurement and optimization.[2]
Sustainable transport systems make a positive contribution to the environmental, social and economic sustainability of the communities they serve. Transport systems exist to provide social and economic connections, and people quickly take up the opportunities offered by increased mobility.[3] The advantages of increased mobility need to be weighed against the environmental, social and economic costs that transport systems pose.
Transport systems have significant impacts on the environment, accounting for between 20% and 25% of world energy consumption and carbon dioxide emissions.[4] The majority of the emissions, almost 97%, came from direct burning of fossil fuels.[5] Greenhouse gas emissions from transport are increasing at a faster rate than any other energy using sector.[6] Road transport is also a major contributor to local air pollution and smog.[7]
The social costs of transport include road crashes, air pollution, physical inactivity,[8] time taken away from the family while commuting and vulnerability to fuel price increases. Many of these negative impacts fall disproportionately on those social groups who are also least likely to own and drive cars.[9] Traffic congestion imposes economic costs by wasting people's time and by slowing the delivery of goods and services.
Traditional transport planning aims to improve mobility, especially for vehicles, and may fail to adequately consider wider impacts. But the real purpose of transport is access - to work, education, goods and services, friends and family - and there are proven techniques to improve access while simultaneously reducing environmental and social impacts, and managing traffic congestion.[10] Communities which are successfully improving the sustainability of their transport networks are doing so as part of a wider programme of creating more vibrant, livable, sustainable cities.
Contents
1 Definition
2 History
3 Environmental impact
4 Transport and social sustainability
5 Cities
6 Policies and governance
6.1 Community and grassroots action
7 Recent trends
8 Environmental calculator of the French environment and energy agency
8.1 Analysis
8.2 Results
9 Criticism
10 Objectives
11 See also
12 Notes and references
13 Bibliography
14 External links
Definition[edit]
The term sustainable transport came into use as a logical follow-on from sustainable development, and is used to describe modes of transport, and systems of transport planning, which are consistent with wider concerns of sustainability. There are many definitions of the sustainable transport, and of the related terms sustainable transportation and sustainable mobility.[11] One such definition, from the European Union Council of Ministers of Transport, defines a sustainable transportation system as one that:
Allows the basic access and development needs of individuals, companies and society to be met safely and in a manner consistent with human and ecosystem health, and promotes equity within and between successive generations.
Is Affordable, operates fairly and efficiently, offers a choice of transport mode, and supports a competitive economy, as well as balanced regional development.
Limits emissions and waste within the planet’s ability to absorb them, uses renewable resources at or below their rates of generation, and uses non-renewable resources at or below the rates of development of renewable substitutes, while minimizing the impact on the use of land and the generation of noise.
Sustainability extends beyond just the operating efficiency and emissions. A Life-cycle assessment involves production, use and post-use considerations. A cradle-to-cradle design is more important than a focus on a single factor such as energy efficiency.[12][13]
History[edit]
An 1889 Japanese print shows various forms of transportation
Most of the tools and concepts of sustainable transport were developed before the phrase was coined. Walking, the first mode of transport, is also the most sustainable.[14] Public transport dates back at least as far as the invention of the public bus by Blaise Pascal in 1662.[15] The first passenger tram began operation in 1807 and the first passenger rail service in 1825. Pedal bicycles date from the 1860s. These were the only personal transport choices available to most people in Western countries prior to World War II, and remain the only options for most people in the developing world. Freight was moved by human power, animal power or rail.
The post-war years brought increased wealth and a demand for much greater mobility for people and goods. The number of road vehicles in Britain increased fivefold between 1950 and 1979,[16] with similar trends in other Western nations. Most affluent countries and cities invested heavily in bigger and better-designed roads and motorways, which were considered essential to underpin growth and prosperity. Transport planning became a branch of civil engineering and sought to design sufficient road capacity to provide for the projected level of traffic growth at acceptable levels of traffic congestion - a technique called "predict and provide". Public investment in transit, walking and cycling declined dramatically in the United States, Great Britain and Australia, although this did not occur to the same extent in Canada or mainland Europe.[17][18]
Concerns about the sustainability of this approach became widespread during the 1973 oil crisis and the 1979 energy crisis. The high cost and limited availability of fuel led to a resurgence of interest in alternatives to single occupancy vehicle travel.
Transport innovations dating from this period include high-occupancy vehicle lanes, citywide carpool systems and transportation demand management. Singapore implemented congestion pricing in the late 1970s, and Curitiba began implementing its Bus Rapid Transit system in the early 1980s.
Relatively low and stable oil prices during the 1980s and 1990s led to significant increases in vehicle travel from 1980–2000, both directly because people chose to travel by car more often and for greater distances, and indirectly because cities developed tracts of suburban housing, distant from shops and from workplaces, now referred to as urban sprawl. Trends in freight logistics, including a movement from rail and coastal shipping to road freight and a requirement for just in time deliveries, meant that freight traffic grew faster than general vehicle traffic.
At the same time, the academic foundations of the "predict and provide" approach to transport were being questioned, notably by Peter Newman in a set of comparative studies of cities and their transport systems dating from the mid-1980s.[19]
The British Government's White Paper on Transport[20] marked a change in direction for transport planning in the UK. In the introduction to the White Paper, Prime Minister Tony Blair stated that
We recognise that we cannot simply build our way out of the problems we face. It would be environmentally irresponsible - and would not work.
A companion document to the White Paper called "Smarter Choices" researched the potential to scale up the small and scattered sustainable transport initiatives then occurring across Britain, and concluded that the comprehensive application of these techniques could reduce peak period car travel in urban areas by over 20%.[21]
A similar study[22] by the United States Federal Highway Administration,[23] was also released in 2004 and also concluded that a more proactive approach to transportation demand was an important component of overall national transport strategy.
Environmental impact[edit]
Main articles: Green vehicle and electric bus
The Bus Rapid Transit of Metz uses a diesel-electric hybrid driving system, developed by Belgian Van Hool manufacturer.[24]
Transport systems are major emitters of greenhouse gases, responsible for 23% of world energy-related GHG emissions in 2004, with about three quarters coming from road vehicles. Currently 95% of transport energy comes from petroleum.[6] Energy is consumed in the manufacture as well as the use of vehicles, and is embodied in transport infrastructure including roads, bridges and railways.[25]
The environmental impacts of transport can be reduced by improving the walking and cycling environment in cities, and by enhancing the role of public transport, especially electric rail.[6]
Green vehicles are intended to have less environmental impact than equivalent standard vehicles, although when the environmental impact of a vehicle is assessed over the whole of its life cycle this may not be the case.[26] Electric vehicle technology has the potential to reduce transport CO2 emissions, depending on the embodied energy of the vehicle and the source of the electricity.[27][28] The Online Electric Vehicle (OLEV), developed by the Korea Advanced Institute of Science and Technology (KAIST), is an electric vehicle that can be charged while stationary or driving, thus removing the need to stop at a charging station. The City of Gumi in South Korea runs a 24 km roundtrip along which the bus will receive 100 kW (136 horsepower) electricity at an 85% maximum power transmission efficiency rate while maintaining a 17 cm air gap between the underbody of the vehicle and the road surface. At that power, only a few sections of the road need embedded cables.[29] Hybrid vehicles, which use an internal combustion engine combined with an electric engine to achieve better fuel efficiency than a regular combustion engine, are already common. Natural gas is also used as a transport fuel. Biofuels are a less common, and less promising, technology; Brazil met 17% of its transport fuel needs from bioethanol in 2007, but the OECD has warned that the success of biofuels in Brazil is due to specific local circumstances; internationally, biofuels are forecast to have little or no impact on greenhouse emissions, at significantly higher cost than energy efficiency measures.[30]
In practice there is a sliding scale of green transport depending on the sustainability of the option. Green vehicles are more fuel-efficient, but only in comparison with standard vehicles, and they still contribute to traffic congestion and road crashes. Well-patronised public transport networks based on traditional diesel buses use less fuel per passenger than private vehicles, and are generally safer and use less road space than private vehicles.[17] Green public transport vehicles including electric trains, trams and electric buses combine the advantages of green vehicles with those of sustainable transport choices. Other transport choices with very low environmental impact are cycling and other human-powered vehicles, and animal powered transport. The most common green transport choice, with the least environmental impact is walking.
Transport and social sustainability[edit]
A tram in Melbourne, Australia
Cities with overbuilt roadways have experienced unintended consequences, linked to radical drops in public transport, walking, and cycling. In many cases, streets became void of “life.” Stores, schools, government centers and libraries moved away from central cities, and residents who did not flee to the suburbs experienced a much reduced quality of public space and of public services. As schools were closed their mega-school replacements in outlying areas generated additional traffic; the number of cars on US roads between 7:15 and 8:15 a.m. increases 30% during the school year.[31]
Yet another impact was an increase in sedentary lifestyles, causing and complicating a national epidemic of obesity, and accompanying dramatically increased health care costs.[8][32]
Cities[edit]
Futurama, an exhibit at the 1939 New York World's Fair, was sponsored by General Motors and showed a vision of the City of Tomorrow.
Main article: Transit-oriented development
Cities are shaped by their transport systems. In The City in History, Lewis Mumford documented how the location and layout of cities was shaped around a walkable center, often located near a port or waterway, and with suburbs accessible by animal transport or, later, by rail or tram lines.
In 1939, the New York World's Fair included a model of an imagined city, built around a car-based transport system. In this "greater and better world of tomorrow", residential, commercial and industrial areas were separated, and skyscrapers loomed over a network of urban motorways. These ideas captured the popular imagination, and are credited with influencing city planning from the 1940s to the 1970s.[33]
Interstate 10 and Interstate 45 near downtown Houston, Texas
The popularity of the car in the post-war era led to major changes in the structure and function of cities.[34] There was some opposition to these changes at the time. The writings of Jane Jacobs, in particular The Death and Life of Great American Cities provide a poignant reminder of what was lost in this transformation, and a record of community efforts to resist these changes. Lewis Mumford asked "is the city for cars or for people?"[35] Donald Appleyard documented the consequences for communities of increasing car traffic in "The View from the Road" (1964) and in the UK, Mayer Hillman first published research into the impacts of traffic on child independent mobility in 1971.[36] Despite these notes of caution, trends in car ownership,[16] car use and fuel consumption continued steeply upward throughout the post-war period.
Mainstream transport planning in Europe has, by contrast, never been based on assumptions that the private car was the best or only solution for urban mobility. For example, the Dutch Transport Structure Scheme has since the 1970s required that demand for additional vehicle capacity only be met "if the contribution to societal welfare is positive", and since 1990 has included an explicit target to halve the rate of growth in vehicle traffic.[37] Some cities outside Europe have also consistently linked transport to sustainability and to land-use planning, notably Curitiba, Brazil, Portland, Oregon and Vancouver, Canada.
Greenhouse gas emissions from transport vary widely, even for cities of comparable wealth. Source: UITP, Mobility in Cities Database
There are major differences in transport energy consumption between cities; an average U.S. urban dweller uses 24 times more energy annually for private transport than a Chinese urban resident, and almost four times as much as a European urban dweller. These differences cannot be explained by wealth alone but are closely linked to the rates of walking, cycling, and public transport use and to enduring features of the city including urban density and urban design.[38]
A bypass the Old Town in Szczecin, Poland
The cities and nations that have invested most heavily in car-based transport systems are now the least environmentally sustainable, as measured by per capita fossil fuel use.[38] The social and economic sustainability of car-based urban planning has also been questioned. Within the United States, residents of sprawling cities make more frequent and longer car trips, while residents of traditional urban neighbourhoods make a similar number of trips, but travel shorter distances and walk, cycle and use transit more often.[39] It has been calculated that New York residents save $19 billion each year simply by owning fewer cars and driving less than the average American.[40] A less car intensive means of urban transport is carsharing, which is becoming popular in North America and Europe, and according to The Economist, carsharing can reduce car ownership at an estimated rate of one rental car replacing 15 owned vehicles.[41] Car sharing has also begun in the developing world, where traffic and urban density is often worse than in developed countries. Companies like Zoom in India, eHi in China, and Carrot in Mexico, are bringing car-sharing to developing countries in an effort to reduce car-related pollution, ameliorate traffic, and expand the number of people who have access to cars.[42]
The European Commission adopted the Action Plan on urban mobility on 2009-09-30 for sustainable urban mobility. The European Commission will conduct a review of the implementation of the Action Plan in the year 2012, and will assess the need for further action. In 2007, 72% of the European population lived in urban areas, which are key to growth and employment. Cities need efficient transport systems to support their economy and the welfare of their inhabitants. Around 85% of the EU’s GDP is generated in cities. Urban areas face today the challenge of making transport sustainable in environmental (CO2, air pollution, noise) and competitiveness (congestion) terms while at the same time addressing social concerns. These range from the need to respond to health problems and demographic trends, fostering economic and social cohesion to taking into account the needs of persons with reduced mobility, families and children.[43]
Policies and governance[edit]
See also: urban sprawl
Sustainable transport policies have their greatest impact at the city level. Outside Western Europe, cities which have consistently included sustainability as a key consideration in transport and land use planning include Curitiba, Brazil; Bogota, Colombia; Portland, Oregon; and Vancouver, Canada. The state of Victoria, Australia passed legislation in 2010 - the Transport Integration Act[44] - to compel its transport agencies to actively consider sustainability issues including climate change impacts in transport policy, planning and operations.[45]
Many other cities throughout the world have recognised the need to link sustainability and transport policies, for example by joining Cities for Climate Protection.[46]
Oil price trend, 1939–2007, both nominal and adjusted to inflation.
Vehicle-miles traveled in the United States up to March 2009.
Community and grassroots action[edit]
Sustainable transport is fundamentally a grassroots movement, albeit one which is now recognised as of citywide, national and international significance.
Whereas it started as a movement driven by environmental concerns, over these last years there has been increased emphasis on social equity and fairness issues, and in particular the need to ensure proper access and services for lower income groups and people with mobility limitations, including the fast-growing population of older citizens. Many of the people exposed to the most vehicle noise, pollution and safety risk have been those who do not own, or cannot drive cars, and those for whom the cost of car ownership causes a severe financial burden.[47]
An organization called Greenxc started in 2011 created a national awareness campaign in the United States encouraging people to carpool by ride-sharing cross country stopping over at various destinations along the way and documenting their travel through video footage, posts and photography.[48] Ride-sharing reduces individual's carbon footprint by allowing several people to use one car instead of everyone using individual cars.
Recent trends[edit]
Car travel increased steadily throughout the twentieth century, but trends since 2000 have been more complex. Oil price rises from 2003 have been linked to a decline in per capita fuel use for private vehicle travel in the USA,[49] Britain and Australia. In 2008, global oil consumption fell by 0.8% overall, with significant declines in consumption in North America, Western Europe, and parts of Asia.[50] Other factors affecting a decline in driving, at least in America, include the retirement of Baby Boomers who now drive less, preference for other travel modes (such as transit) by younger age cohorts, the Great Recession, and the rising use of technology (internet, mobile devices) which have made travel less necessary and possibly less attractive.[51]
Environmental calculator of the French environment and energy agency[edit]
The environmental calculator of the French environment and energy agency ( ADEME)[52] enables one to compare the different means of transportation as regards the CO2 emissions (in terms of carbon dioxide equivalent) as well as the consumption of primary energy. In the case of an electric vehicle, the ADEME makes the assumption that 2.58 toe as primary energy are necessary for producing one toe of electricity as end energy in France (see Embodied energy#Embodied energy in the energy field).
This computer tool devised by the ADEME shows the importance of public transportation from an environmental point of view. It highlights the primary energy consumption as well as the CO2 emissions due to transportation. However, nothing is said about the production of radwaste. Moreover, intermodal passenger transport is probably a key to sustainable transport, by allowing people to use less polluting means of transportation.
In fact, the high efficiency of the public transportation will not suffice. In fact, the Negawatt approach should be drawn on, and sustainable transportation rests on two other pillars which are sufficiency and renewable energies.
Analysis[edit]
Comparing different means of transportation in terms of energy consumption turns out to be extremely difficult. That is the reason why we are making use of the ADEME environmental calculator. However, in order to make things simpler, we will be using kWh and equivalent liter of gasoline per 100 km (1 l gasoline = 9.85 kWh). The figures given in the table will always correspond to a primary energy.
Biologists, who happen to be biochemists and biomechanics engineers as well, teach us that, depending of the air quantity which has been breathed, the consumed energy for walking, riding a bike or running can be evaluated[53] even if the muscle efficiency would amount to about 20%.
The energy consumption of an electric car would amount to 10 till 20 kWh per 100 km.[54][55] While the electric consumption of an e-Golf is said to average 12.7 kWh per 100 km at the electric plug, according to Volkswagen, the value measured by the German automotive organization ADAC is rather 18.2 kWh per 100 km.[56]
Using the environmental calculator of the French ADEME, and assuming that the consumption of an electric car at the plug is 18.2 kWh per 100 km (with an efficiency of 35.8%, see Embodied energy#Embodied energy in the energy field) enables us to fill in the following table:
Energy effiency of a car
On average, the efficiency of internal combustion vehicles is bad, as explained by the nearby sketch. It is possible to do better, with smaller cars. As a matter of fact, rolling resistance (above all due to the tyres) decreases, as well as kinetic energy. Since smaller cars generally drive more slowly, the impact of automotive aerodynamics will be lower too.
Rolling resistance on rails turns out to be much more efficient in terms of rolling resistance than tyres. Moreover, a train made up of many passenger cars shows on average a low aerodynamic resistance (in the table, the good efficiency of the very swift TGV is a proof of it).
Now, let us suppose that all the French electricity is produced in the framework of cogeneration, which is of course a wrong assumption. Cogeneration shows an efficiency of about 80% whereas the efficiency of the grid can be estimated at 92.5%, leading to an averall efficiency of 74%. In the table, we would have to replace 1 / 2.58 (i.e.38.8 %) by 74%. And the table would become:
Remark:
The unit kWh/100 km is analogous to force of 36 N.
Limits of the study:
the embodied energy was not taken into account (see Embodied_energy#Embodied_energy_in_automobiles).
Hybrid cars have not been studied. It will be difficult because many forms of energy are at stake.
Results[edit]
Public transportation on rail boasts the best results in terms of energy efficiency in transportation. Sustainable transport can be achieved through intermodal passenger transport. It is confirmed by the convergence of the ADEME,[57] GrDF (French gas networks)[58] as well as Greenpeace-France[59] and association négaWatt scenarios which all foresee a shift from road to rail for journeys of 100 km and upwards. Planes, which can cover very long distances are not part of sustainable transport. Remark:
The excellent energy efficiency of transport on rail applies also to freight transport on rail.[60] The reason why a railway network still exists in the 21st century is that it boasts an excellent energy efficiency.
Criticism[edit]
The term Green transport is often used as a greenwash marketing technique for products which are not proven to make a positive contribution to environmental sustainability. Such claims can be legally challenged. For instance Norway's consumer ombudsman has targeted automakers who claim that their cars are "green", "clean" or "environmentally friendly". Manufacturers risk fines if they fail to drop the words.[61] The Australian Competition and Consumer Commission (ACCC) describes green claims on products as very vague, inviting consumers to give a wide range of meanings to the claim, which risks misleading them.[62] In 2008 the ACCC forced a car retailer to stop its green marketing of Saab cars, which was found by the Australian Federal Court as misleading.[63]
Objectives[edit]
The EU Directorate-General for Transport and Energy (DG-TREN) has launched a programme which focusses mostly on Urban Transport. Its main measures are:
See also[edit]
Fuel efficiency in transportation
Alternatives to the automobile
Sustainable aviation fuel
Environmental impact of aviation
EcoMobility Alliance
Hypermobility
ITDP
Michael Replogle
Michelin Challenge Bibendum
UITP
Notes and references[edit]
^ Jeon, C M; Amekudzi (March 2005), "Addressing Sustainability in Transportation Systems: Definitions, Indicators, and Metrics" (PDF), JOURNAL OF INFRASTRUCTURE SYSTEMS: 31–50
^ Helping to Build a Safe and Sustainable Transportation Infrastructure (PDF), U.S. Department of Transportation’s Research and Innovative Technology Administration, May 2010
^ Schafer, A. (1998) "The global demand for motorized mobility." Transportation Research A 32(6), 455-477.
^ ">World Energy Council (2007). "Transport Technologies and Policy Scenarios". World Energy Council. Retrieved 2009-05-26.
^ "About Transportation & Climate Change: Transportation’s Role in Climate Change: Overview - DOT Transportation and Climate Change Clearinghouse". climate.dot.gov. Retrieved 2015-11-15.
^ a b c Intergovernmental Panel on Climate Change (2007). "IPCC Fourth Assessment Report: Mitigation of Climate Change, chapter 5, Transport and its Infrastructure" (PDF). Intergovernmental Panel on Climate Change. Retrieved 2009-05-26.
^ "National multipollutant emissions comparison by source sector in 2002". US Environmental Protection Agency. 2002. Retrieved 2009-03-18.
^ a b World Health Organisation, Europe. "Health effects of transport". Retrieved 2008-08-29.  Archived April 30, 2010, at the Wayback Machine.
^ Social Exclusion Unit, Office of the Prime Minister (UK). "Making the Connections - final report on transport and social exclusion" (PDF). Retrieved 2003-02-01. [dead link]
^ Todd Litman (1998). "Measuring Transportation: Traffic, Mobility and Accessibility" (PDF). Victoria Transport Policy Institute. Retrieved 2009-03-18.  External link in |publisher= (help)
^ Todd Litman (2009). "Sustainable Transportation and TDM". Online TDM Encyclopedia. Victoria Transport Policy Institute. Retrieved 2009-04-07.  External link in |publisher= (help)
^ Strategies for Managing Impacts from Automobiles, US EPA Region 10, retrieved May 22, 2012
^ "European Union’s End-of-life Vehicle (ELV) Directive", End of Life Vehicles (EU), retrieved May 22, 2012
^ "Walking benefits". Transport for London. Retrieved 2009-05-16.
^ "March 18, 1662: The Bus Starts Here ... in Paris". Wired. 2008-03-18. Retrieved 2009-05-16.
^ a b "Transport Statistics Great Britain 2008: Section 9, Vehicles" (PDF). Archived from the original (PDF) on June 6, 2011. Retrieved 2009-03-18.
^ a b "Making Transit Work: insight from Western Europe, Canada and the United States" (PDF). Transportation Research Board. 2001. Retrieved 2008-07-22.
^ "Promoting Safe Walking and Cycling to Improve Public Health:Lessons from The Netherlands and Germany" (PDF). American Journal of Public Health, Vol. 93. Retrieved 2008-08-30.
^ Cities and Automobile Dependence: An International Sourcebook, Newman P and Kenworthy J, Gower, Aldershot, 1989
^ "White Paper on Transport". 2004. Retrieved 2009-07-04.  Archived February 6, 2010, at the Wayback Machine.
^ Cairns, S; et al. (July 2004). "Smarter Choices, Changing the Way we Travel page v". Archived from the original on June 14, 2007. Retrieved 2008-07-27.
^ [ similar study]
^ "Mitigating Traffic Congestion". 2004. Retrieved 2009-07-04.
^ "Van Hool presents the ExquiCity Design Mettis.". Retrieved 5 June 2012.
^ Joshua M. Pearce, Sara J. Johnson, and Gabriel B. Grant Resources, Conservation and Recycling, 51 pp. 435-453, 2007. "3D-Mapping Optimization of Embodied Energy of Transportation". Retrieved 2010-04-31.  line feed character in |author= at position 56 (help); Check date values in: |access-date= (help)
^ Heather L. MacLean and Lester B. Lave University of Toronto. "OECD’s Economic Assessment of Biofuel Support Policies". Retrieved 2010-04-31.  line feed character in |author= at position 38 (help); Check date values in: |access-date= (help)
^ Brinkman, Norman; Eberle, Ulrich; Formanski, Volker; Grebe, Uwe-Dieter; Matthe, Roland (2012-04-15). "Vehicle Electrification - Quo Vadis". VDI. Retrieved 2013-04-27.
^ Clive Matthew-Wilson. "The Emperor's New Car" (PDF). Retrieved 2010-04-31.  Check date values in: |access-date= (help)
^ http://www.kaist.edu/english/01_about/06_news_01.php?req_P=bv&req_BIDX=10&req_BNM=ed_news&pt=17&req_VI=4404
^ OECD. "OECD’s Economic Assessment of Biofuel Support Policies". Retrieved 2009-07-31.
^ U.S. Centers for Disease Control and Prevention. "Active Transportation to School Then and Now — Barriers and Solutions". KidsWalk-to-School: Resource Materials - DNPAO - CDC. Retrieved 2008-07-22.
^ "An interview with Dr Reid Ewing". Relationship between urban sprawl and physical activity, obesity, and morbidity. American Journal of Health Promotion 18[1]: 47-57. September–October 2003. Retrieved 2008-07-25.
^ Ellis, Cliff. "Lewis Mumford and Norman Bel Geddes: the highway, the city and the future". Planning Perspectives 20 (1): 51–68. doi:10.1080/0266543042000300537. Retrieved 2009-06-14.
^ James Howard Kunstler (1993). The Geography of Nowhere.
^ Lewis Mumford. "Lewis Mumford on the City". Retrieved 2009-03-18.
^ Hillman, Mayer. "Children Key publications". Key publicatonson children's quality of life by Dr. Mayer Hillman. Retrieved 2009-03-18.
^ van den Hoorn, T and B van Luipen (2003). "National and Regional Transport Policy in the Netherlands" (PDF). Retrieved 2008-07-27.
^ a b Kenworthy, J R Transport Energy Use and Greenhouse Emissions in Urban Passenger Transport Systems : A Study of 84 Global Cities Murdoch University
^ Ewing, R and R Cervero (2001). "Travel and the Built Environment: A Synthesis" (PDF). Transportation Research Record, 1780: 87-114. 2001. Transportation Research Record 1780,87-114. Retrieved 2008-07-22.
^ "New York City's Green Dividend" (PDF). CEOs for Cities. 2009. Retrieved 2010-05-15.
^ "Seeing the back of the car". The Economist. 2012-09-22. Retrieved 2012-09-23.  Published in the Sept 22nd 2012 print edition.
^ "How are social enterprises helping address road safety and transportation challenges in India?".
^ Transport: Action Plan on urban mobility - European commission
^ Transport Integration Act 2010
^ Transport Integration Act 2010, Part 2 - see http://www.legislation.vic.gov.au.
^ "ICLEI Local Governments for Sustainability". Cities for Climate Protection. International Council for Local Environmental Initiatives. 1995–2008. Retrieved 2009-03-18.
^ "Making the Connections: Final report on transport and social exclusion". UK Social Exclusion Unit. February 2003. Retrieved 2008-07-22.
^ "GreenXC". GreenXC Website.
^ "Transportation Energy Data Book". US Department of Energy. 2009. Retrieved 2010-05-14.
^ "BP Statistical Review of World Energy 2009". BP. 2009. Retrieved 2010-05-14.
^ "A New Direction". 2013. U.S. PIRG. Retrieved 15 October 2013.
^ (fr) ADEME environmental calculator which informs about the CO2 emissions and the primary energy consumption
^ (fr) Energy consumption of different means of transportation website Agoravox.
^ (fr) electric cars website challenges.fr
^ (fr) electric cars website global-chance.org
^ (de) VW_e_Golf.pdf website ADAC.de see page 12
^ (fr) ADEME scenario see page 25.
^ (fr) GrDF scenario see page 14.
^ (fr) Greenpeace-France scenario see page 15.
^ freight transport on rail website illinois.edu see page 6.
^ "Norways Says Cars Neither Green Nor Clean". Reuters.com. 2007-09-06. Retrieved 2009-10-04.
^ ACCC: Green marketing and the Trade Practices Act, 2008. Retrieved 2009-10-04.
^ 'Drive.com.au: Coming clean on green. Retrieved 2009-10-04.
Bibliography[edit]
Sustainability and Cities: Overcoming Automobile Dependence, Island Press, Washington DC, 1999. Newman P and Kenworthy J, ISBN 1-55963-660-2.
Sustainable Transportation Networks, Edward Elgar Publishing, Cheltenham, England, 2000. Nagurney A, ISBN 1-84064-357-9
Introduction to Sustainable Transportation: Policy, Planning and Implementation, Earthscan, London, Washington DC, 2010. Schiller P Eric C. Bruun and Jeffrey R. Kenworthy, ISBN 978-1-84407-665-9.
Sustainable Transport, Mobility Management and Travel Plans, Ashgate Press, Farnham, Surrey, 2012, Enoch M P. ISBN 978-0-7546-7939-4.
External links[edit]
Sustainable Mobility Initiative (National Renewable Energy Laboratory)
Sustainable Transportation Research (National Renewable Energy Laboratory)
Efficient Mobility Summit: Transportation and the Future of Dynamic Mobility Systems, January 2016 (National Renewable Energy Laboratory)
Share & lend bicycles across the globe, for free
A Global High Shift Scenario: Impacts And Potential For More Public Transport, Walking, And Cycling With Lower Car Use, Institute for Transportation and Development Policy, September 2014
Guiding Principles to Sustainable Mobility
Sustainable Urban Transport Project - knowledge platform (SUTP)
Capacity Building for Sustainable Urban Transport - open platform for training and e-learning offers (hosted by SUTP)
German Partnership for Sustainable Mobility (GPSM)
Institute for Transportation and Development Policy (ITDP)
EMBARQ - The WRI Center for Sustainable Transportation
Institute for Sustainability and Technology Policy
Bridging the Gap: Pathways for transport in the post 2012 process
Sustainable-mobility.org: the centre of resources on sustainable transport
QUEST project: Quality Management tool for Urban Energy efficient Sustainable Transport
EcoMobility Alliance: An ICLEI - Local Governments for Sustainability Initiative on Transport
Sustainable Transportation in the Middle East via Carboun
The Ecoliner Fairwinds, a proposed large sailing container vessel
The Greenheart, a proposed small sailing container vessel
Transport Action Plan: Urban Electric Mobility Initiative, United Nations, Climate Summit 2014, September 2014
Turning the right corner : ensuring development through a low-carbon transport sector, World Bank Group, May 2013.
Is this the future? A more sustainable inner city delivery system
Improve sustainability of delivery fleets
Transportation Research at IssueLab
Retrieved from "https://en.wikipedia.org/w/index.php?title=Sustainable_transport&oldid=707420263"
Categories: Sustainable transportHidden categories: All articles with dead external linksArticles with dead external links from September 2010CS1 errors: external linksCS1 errors: invisible charactersCS1 errors: dates