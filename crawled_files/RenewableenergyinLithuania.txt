Renewable energy in Lithuania - Wikipedia, the free encyclopedia
Renewable energy in Lithuania
Kruonis Pumped Storage Plant
Wind turbine in Lithuania
Construction of wind and solar energy near Alytus.
In 2010 Renewable energy in Lithuania constituted 19.7% of the country's overall electricity generation.[1] The Lithuanian government aims to generate 23% of total power from renewable resources by 2020.
Contents
1 Statistics
2 Biomass
3 Biofuel
3.1 Biogas
4 Hydroelectricity
5 Geothermal energy
6 Solar power
7 Wind power
8 See also
9 References
10 External links
Statistics[edit]
Renewable energy in Lithuania by type:[2]
Biomass[edit]
Main article: Biomass in Lithuania
Biomass represents the most common source of renewable energy in Lithuania, with most of the biomass is use being firewood. The amount of energy generated from biomass in Lithuania is the second highest in the EU per capita. It is estimated that in 2020 the country will lead the EU in the quantity of biomass available for biofuel production.
Biofuel[edit]
Main article: Biofuel in Lithuania
Biogas[edit]
Hydroelectricity[edit]
Main article: Hydroelectric power in Lithuania
Kruonis Pumped Storage Plant, its main purpose is to provide a spinning reserve of the power system, to regulate the load curve of the power system 24 hours a day. Installed capacity of the pumped storage plant: 900 MW (4 units, 225 MW each).
Kaunas Hydroelectric Power Plant, it supplies about 3% of the electrical demand in Lithuania.[5]
Geothermal energy[edit]
Main article: Geothermal energy in Lithuania
Klaipėda Geothermal Demonstration Plant, the first geothermal heating plant in the Baltic Sea region.[6]
Solar power[edit]
Main article: Solar power in Lithuania
Solar power in Lithuania created 2.4 MWh power in 2010.[7] At the start of 2014 Lithuania had capacity of 61 MW of solar power.[8]
Wind power[edit]
Main article: Wind power in Lithuania
Installed wind power capacity in Lithuania and generation in recent years is shown in the table below:[9][10][11][12][13]
See also[edit]
Energy in Lithuania
References[edit]
^ [1]
^ "Energetikos statistika" (in Lithuanian). stat.gov.lt. 2012-06-15. Retrieved 2012-06-16.
^ [Lithuanian Renewable Energy Promotion Action Plan 2010-2020 years. 2008. Applied research. Vilnius. 215]
^ Biofuels barometer 2007 – EurObserv’ER Systèmes solaires Le journal des énergies renouvelables n° 179, s. 63–75, 5/2007
^ "Kauno HE modernizavimas" (in Lithuanian). Lietuvos Energija. Retrieved 2008-01-09.
^ "Implementation Completion Report". The World Bank. 2005. p. 4. Retrieved 2008-05-04.
^ "Energetikos statistika 2010 m. keitėsi šalies kuro ir energijos sąnaudų struktūra" (in Lithuanian). stat.gov.lt. 2011-06-05. Retrieved 2012-06-10.
^ Instaliuota gala
^ "Wind in power - 2009 European statistics" (pdf). ewea.org. February 2010. Retrieved 2012-06-06.
^ "Wind in power - 2010 European statistics" (pdf). ewea.org. February 2011. Retrieved 2012-06-06.
^ "Wind in power - 2011 European statistics" (pdf). ewea.org. February 2012. Retrieved 2012-06-06.
^ "Wind in power in Europe 1998-2009" (XLS). ewea.org. February 2009. Retrieved 2012-06-06.
^ [2]
External links[edit]
Media related to Renewable energy in Lithuania at Wikimedia Commons
Retrieved from "https://en.wikipedia.org/w/index.php?title=Renewable_energy_in_Lithuania&oldid=695412857"
Categories: Renewable energy in LithuaniaHidden categories: CS1 Lithuanian-language sources (lt)