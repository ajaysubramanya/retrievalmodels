Greenhouse Solutions with Sustainable Energy - Wikipedia, the free encyclopedia
Greenhouse Solutions with Sustainable Energy
The wind, Sun, and biomass are three renewable energy sources
Greenhouse Solutions with Sustainable Energy is a 2007 book by Australian academic Mark Diesendorf. The book puts forward a set of policies and strategies for implementing the most promising clean energy technologies by all spheres of government, business and community organisations. Greenhouse Solutions with Sustainable Energy suggests that a mix of efficient energy use, renewable energy sources and natural gas (as a transitional fuel) offers a clean and feasible energy future for Australia.[1]
Contents
1 Structure and themes
2 Wind power variability
3 Quotes
4 Critical reception
5 Author
6 See also
7 References
8 Bibliography
Structure and themes[edit]
The book is a comprehensive guide to sustainable energy systems[2] and is structured in three sections:
Introduction to the basic concepts and latest scientific evidence regarding global warming.
Assessment of energy technologies, including coal, nuclear and more sustainable alternatives.
Discussion of policies and strategies needed to overcome the non-technical barriers to renewable energies and energy efficiency.[3]
Diesendorf argues that:
Ecologically sustainable energy technologies based on energy efficiency, renewable energy and natural gas are commercially available today, and that their implementation could halve Australia’s greenhouse gas emissions within a few decades.
To implement these technologies, new policies must be developed and implemented by all three levels of government.
The main barriers are neither technical nor economic, but rather our social institutions and the political power of the big greenhouse gas emitting industries: coal, oil, aluminium, cement and motor vehicles.[1]
Wind power variability[edit]
Early in his career, Mark Diesendorf was a principal research scientist with CSIRO where he was involved in early research on integrating wind power into electricity grids. This issue is discussed in some detail in Greenhouse Solutions with Sustainable Energy.
Diesendorf explains that large-scale wind power is not "intermittent", because it does not start up or switch off instantaneously. In practice, the variations in thousands of wind turbines, spread out over several different sites and wind regimes, are smoothed. As the distance between sites increases, the correlation between wind speeds measured at those sites, decreases. This has been confirmed recently by studies conducted by Graham Sinden from Oxford University:[4]
[Graham Sinden] analysed over 30 years of hourly wind speed data from 66 sites spread out over the United Kingdom. He found that the correlation coefficient of wind power fell from 0.6 at 200 km to 0.25 at 600 km separation (a perfect correlation would have a coefficient equal to 1.0.) There were no hours in the data set where wind speed was below the cut-in wind speed of a modern wind turbine throughout the United Kingdom, and low wind speed events affecting more than 90 per cent of the United Kingdom had an average recurrent rate of only one hour per year.[4]
Diesendorf goes on to say that every conventional power station breaks down unexpectedly from time to time, causing an immediate loss of all its power. That is true intermittency, according to Diesendorf, and it is a particular type of variability that switches between full power and no power. Once a conventional power station has broken down, it may be offline for weeks, much longer than windless periods.[5]
Quotes[edit]
"The enhanced greenhouse effect is arguably the most dangerous environmental problem and the most difficult political issue to be faced by the world in the 21st century." (p. 1)
"The recent push for a revival of nuclear energy has been based on its claimed reduction in CO2 emissions where it substitutes for coal-fired power stations. In reality, only reactor operation is CO2-free. All other stages of the nuclear fuel chain -- mining, milling, fuel fabrication, enrichment, reactor construction, decommissioning, and waste management -- use fossil fuels and hence emit CO2..." (p. 252)
"Global wind-power capacity continues to expand and, apart from the blip in 2006, its costs continue to decline steadily. Wind power is one of the few energy supply technologies that are ready for wide dissemination today, unlike coal with CO2 capture and sequestration and unlike nuclear power. Wind can deliver deep cuts in CO2, while providing a hedge against fluctuating fossil fuel prices and reducing energy import dependence." (p. 126)
Critical reception[edit]
Dick Nichols, in Green Left, states that Greenhouse Solutions with Sustainable Energy brings together much useful material about global warming and possible solutions:
Diesendorf's book concentrates into one volume a succinct analysis of global warming, a rebuttal of climate change scepticism, a thorough summary of the state of development of each renewable energy technology, a masterly demolition of false “solutions” to greenhouse (like carbon sequestration and nuclear power) and a presentation of strategies and policies for uprooting carbon-intensive power production in Australia. Add in chapters on saving energy and transport and urban redesign and Greenhouse Solutions with Sustainable Energy illuminates the reader about all the main features of the many-sided debate on how to make sustainable energy production the heart of the attack on climate change.[6]
Nichols suggests that if there’s one serious shortcoming in Greenhouse Solutions with Sustainable Energy it’s "a failure to consistently trace what rate of implementation of energy efficiency and uptake of renewable energy sources is necessary to confront the greenhouse emergency".
Patrick O'Neill, in Chain Reaction, suggests that Greenhouse Solutions with Sustainable Energy is a comprehensive guide to sustainable energy systems, and that the book is "simply a joy to read". O'Neill explains that whilst an immense amount of technical and scientific detail is presented, the language is simple and the book is well laid out. It also covers often ignored areas of the sustainable energy discussion such as population issues, morality, social justice and equity. In conclusion, O'Neill states that the book is a "wonderfully energising piece of sedition", which calls for a "coordinated national strategy for non-violent action".[2]
George Wilkenfield, in Australian Review of Public Affairs, is more critical and suggests the book feels "strangely anachronistic", partly because it treats climate change as an issue that can be resolved through grassroots activism or by the opposition of protest movements to unjust regimes. Wilkenfield explains that "this rather misses the point that the continuing rise in global greenhouse gas emissions is not a byproduct of injustice or repression but of economic freedom in the West and the unrelenting economic growth by which repressive regimes such as China’s buy legitimacy. In general, very few people want to be freed from consumption—most want the freedom to consume even more". Wilkenfield goes on to say that Diesendorf misses some of the dynamics of consumption: energy use gives people comfort, speed, privacy and convenience. Many people actually enjoy driving and like their cars, and would continue to use them as long as they could afford to, even if public transport were available.[7]
Nevertheless, in the end, Wilkenfield concedes that Greenhouse Solutions with Sustainable Energy is very suitable as a textbook, or as a handbook for activists, journalists or the more committed reader who is not afraid of numbers.[7]
Author[edit]
Mark Diesendorf teaches and researches ecologically sustainable development and greenhouse solutions at the Institute of Environmental Studies at the University of NSW. Previously he has been a Principle Research Scientist at CSIRO, Professor of Environmental Science at UTS and Vice-President of the Australia New Zealand Society for Ecological Economics.[8]
See also[edit]
Anti-nuclear movement in Australia
Clean Tech Nation
Effects of global warming on Australia
List of Australian environmental books
Reaction Time (book)
Renewable energy commercialization in Australia
Solar power in Australia
Sustainable development
Wind power in Australia
References[edit]
^ a b UNSW Press. : Greenhouse Solutions with Sustainable Energy
^ a b Greenhouse Solutions with Sustainable Energy: Review by Patrick O'Neill Chain Reaction 100, August 2007.
^ Scientific approach to an emotional – and political – issue ECOS, June–July 2007, p.31.
^ a b Diesendorf, Mark (2007). Greenhouse Solutions with Sustainable Energy, p. 119.
^ Diesendorf, Mark (2007). Greenhouse Solutions with Sustainable Energy, p. 118.
^ Dick Nichols, How can sustainable energy solve greenhouse? Green Left, 5 April 2008.
^ a b George Wilkenfield, Cutting greenhouse emissions—What would we do if we really meant it? Australian Review of Public Affairs, August 2007.
^ Greenhouse Solutions with Sustainable Energy -- Free symposium
Bibliography[edit]
Diesendorf, Mark (2007). Greenhouse Solutions with Sustainable Energy, UNSW Press, 432 pages, ISBN 978-0-86840-973-3
Retrieved from "https://en.wikipedia.org/w/index.php?title=Greenhouse_Solutions_with_Sustainable_Energy&oldid=696246016"
Categories: 2007 books2007 in the environmentAustralian non-fiction booksSustainability booksBooks about energy issuesClimate change books