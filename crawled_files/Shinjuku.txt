Shinjuku - Wikipedia, the free encyclopedia
Shinjuku
For other uses, see Shinjuku (disambiguation).
Shinjuku (新宿区, Shinjuku-ku?, "New Lodge") is a special ward in Tokyo, Japan. It is a major commercial and administrative centre, housing the busiest railway station in the world (Shinjuku Station) and the Tokyo Metropolitan Government Building, the administration centre for the government of Tokyo. As of 2015, the ward has an estimated population of 337,556 and a population density of 18,517 people per km². The total area is 18.23 km².[2]
Contents
1 Geography and neighborhoods
2 History
3 Economy
4 Government and politics
4.1 Elections
5 Transportation
5.1 Rail
5.2 Roads
6 Education
6.1 Colleges and universities
6.2 Schools
7 Public institutions
7.1 Libraries
7.2 Hospitals
7.3 Cultural centers
7.3.1 Museums
7.3.2 Halls
8 Sister cities
9 See also
10 References
11 External links
Geography and neighborhoods[edit]
Man with guitar immediately south of the Shinjuku JR Station, a popular busking location
Shinjuku is surrounded by Chiyoda to the east; Bunkyo and Toshima to the north; Nakano to the west, and Shibuya and Minato to the south.[3]
The current city of Shinjuku grew out of several separate towns and villages, which have retained some distinctions despite growing together as part of the Tokyo metropolis.
East Shinjuku: The area east of Shinjuku Station and surrounding Shinjuku-sanchome Station, historically known as Naito-Shinjuku, houses the city hall and the flagship Isetan department store, as well as several smaller areas of interest:
Kabukichō: Tokyo's best-known red-light district, renowned for its variety of bars, restaurants, and sex-related establishments.
Golden Gai: An area of tiny shanty-style bars and clubs. Musicians, artists, journalists, actors and directors gather here, and the ramshackle walls of the bars are literally plastered with film posters.
Shinjuku Gyoen: A large park, 58.3 hectares, 3.5 km in circumference, blending Japanese traditional, English Landscape and French Formal style gardens.
Shinjuku Ni-chōme: Tokyo's best-known gay district.
Nishi-Shinjuku: The area west of Shinjuku Station, historically known as Yodobashi, is home to Tokyo's largest concentration of skyscrapers. Several of the tallest buildings in Tokyo are located in this area, including the Tokyo Metropolitan Government Building, KDDI Building and Park Tower.
Ochiai: The northwestern corner of Shinjuku, extending to the area around Ochiai-minami-nagasaki Station and the south side of Mejiro Station, is largely residential with a small business district around Nakai Station.
Ōkubo: The area surrounding Okubo Station, Shin-Okubo Station and Higashi-Shinjuku Station is best known as Tokyo's historic ethnic Korean neighborhood.
Totsuka: The northern portion of Shinjuku surrounding Takadanobaba Station and Waseda University. The Takadanobaba area is a major residential and nightlife area for students, as well as a commuter hub.
Ushigome: A largely residential area in the eastern portion of the city.
Ichigaya: A commercial area in eastern Shinjuku, site of the Ministry of Defense.
Kagurazaka: A hill descending to the Iidabashi Station area, once one of Tokyo's last remaining hanamachi or geisha districts, and currently known for hosting a sizable French community.[4]
Yotsuya: An upscale residential and commercial district in the southeast corner of Shinjuku. The Arakichō area is well known for its many small restaurants, bars, and izakaya.
"Shinjuku" is often popularly understood to mean the entire area surrounding Shinjuku Station, but the Shinjuku Southern Terrace complex and the areas to the west of the station and south of Kōshū Kaidō are part of the Yoyogi district of the special ward of Shibuya.
Naturally, most of Shinjuku is occupied by the Yodobashi Plateau, the most elevated portion of which extends through most of the Shinjuku Station area. The Kanda River runs through the Ochiai and Totsuka areas near sea level, but the Toshima Plateau also builds elevation in the northern extremities of Totsuka and Ochiai. The highest point in Shinjuku is Hakone-san in Toyama Park, 44.6 m above sea level.[5]
History[edit]
Shinjuku at night
Street level in Shinjuku
In 1634, during the Edo period, as the outer moat of the Edo Castle was built, a number of temples and shrines moved to the Yotsuya area on the western edge of Shinjuku. In 1698, Naitō-Shinjuku had developed as a new (shin) station (shuku or juku) on the Kōshū Kaidō, one of the major highways of that era. Naitō was the family name of a daimyo whose mansion stood in the area; his land is now a public park, the Shinjuku Gyoen.
In 1920, the town of Naitō-Shinjuku, which comprised large parts of present-day Shinjuku, parts of Nishi-Shinjuku and Kabukichō was integrated into Tokyo City. Shinjuku began to develop into its current form after the Great Kantō earthquake in 1923, since the seismically stable area largely escaped the devastation. Consequently, West Shinjuku is one of the few areas in Tokyo with many skyscrapers.
The Tokyo air raids from May to August 1945 destroyed almost 90% of the buildings in the area in and around Shinjuku Station.[6] The pre-war form of Shinjuku, and the rest of Tokyo, for that matter, was retained after the war because the roads and rails, damaged as they were, remained, and these formed the heart of the Shinjuku in the post-war construction. Only in Kabuki-cho was a grand reconstruction plan put into action.[7]
The present ward was established on March 15, 1947 with the merger of the former wards of Yotsuya, Ushigome, and Yodobashi. It served as part of the athletics 50 km walk and marathon course during the 1964 Summer Olympics.[8]
In 1991, the Tokyo Metropolitan Government moved from the Marunouchi district of Chiyoda to the current building in Shinjuku. (The Tokyo International Forum stands on the former site vacated by the government.)
Economy[edit]
The area surrounding Shinjuku Station is a major economic hub of Tokyo. Many companies have their headquarters or Tokyo offices in this area, including regional telephone operator NTT East, global camera and medical device manufacturer Olympus Corporation, electronics giant Seiko Epson,[9] fast food chains McDonald's Japan and Yoshinoya,[10] travel agency H.I.S.,[11] Fuji Heavy Industries (Subaru),[12] railway operator Odakyu Electric Railway, construction giant Taisei Corporation,[13] medical equipment manufacturer Nihon Kohden,[14] Enoki Films,[15] navigation software company Jorudan,[16] instant noodle giant Nissin Foods[17] and regional airline Airtransse.[18] The station area also hosts numerous major retailers such as Mitsukoshi, Isetan, Takashimaya, Marui, Bic Camera, Yodobashi Camera and Yamada Denki.
Northeastern Shinjuku has an active publishing industry and is home to the publishers Shinchosha[19] and Futabasha.[20]
Government and politics[edit]
Shinjuku City Office
Like the other wards of Tokyo, Shinjuku has a status equivalent to that of a city. The current mayor is Kenichi Yoshizumi. The ward council (区議会, kugikai?) consists of 38 elected members; the Liberal Democratic Party and New Komeitō Party together currently hold a majority. The Democratic Party of Japan, Japanese Communist Party and the Social Democratic Party are also represented together with four independents. Shinjuku's city office (区役所, kuyakusho?) is located on the southeastern edge of Kabukichō.
Shinjuku is also the location of the metropolitan government of Tokyo. The governor's office, the metropolitan assembly chamber, and all administrative head offices are located in the Tokyo Metropolitan Government Building. Technically, Shinjuku is therefore the prefectural capital of Tokyo; but according to a statement by the governor's office, Tokyo (the – as administrative unit: former – Tokyo City, the area of today's 23 special wards collectively) can usually be considered the capital of Tokyo (prefecture/"Metropolis") for geographical purposes. The Geographical Survey Institute (Kokudo Chiriin) names Tōkyō (the city) as capital of Tōkyō-to (the prefecture/"Metropolis").[21]
Elections[edit]
Shinjuku local election, 2004
Shinjuku mayoral election, 2006
Shinjuku local election, 2007
Transportation[edit]
Further information: Transportation in Greater Tokyo
Shinjuku is a major urban transit hub. Shinjuku Station sees an estimated 3.64 million passengers pass through each day, making it the busiest station in the world. It houses interchanges to three subway lines and three privately owned commuter lines, as well as several JR lines.
Rail[edit]
A list of railway lines passing through and stations located within Shinjuku includes:
JR East
Yamanote Line: Takadanobaba, Shin-Ōkubo, Shinjuku
Chūō Line (Rapid), Chūō-Sōbu Line: Yotsuya, Shinanomachi, Shinjuku, Ōkubo
Saikyō Line, Shōnan-Shinjuku Line: Shinjuku
Tokyo Metro
Marunouchi Line: Yotsuya, Yotsuya-sanchōme, Shinjuku-gyoenmae, Shinjuku-sanchōme, Shinjuku, Nishi-Shinjuku
Yūrakuchō Line: Ichigaya, Iidabashi
Tōzai Line: Kagurazaka, Waseda, Takadanobaba, Ochiai
Fukutoshin Line: Nishi-Waseda, Higashi-Shinjuku, Shinjuku-sanchōme
Namboku Line: Iidabashi, Ichigaya, Yotsuya
Tokyo Metropolitan Bureau of Transportation
Toei Shinjuku Line: Akebonobashi, Shinjuku-sanchōme, Shinjuku
Toei Ōedo Line: Ochiai-Minaminagasaki, Nakai, Nishi-Shinjuku-gochōme, Tochō-mae, Kokuritsu-Kyōgijō, Ushigome-Kagurazaka, Ushigome-Yanagichō, Wakamatsu-Kawada, Higashi-Shinjuku, Shinjuku-Nishiguchi
Toden Arakawa Line: Omokagebashi, Waseda
Odakyu Electric Railway Odawara Line: Shinjuku
Keio Corporation Keio Line, Keio New Line: Shinjuku
Seibu Railway Seibu Shinjuku Line: Seibu-Shinjuku, Takadanobaba, Shimo-Ochiai, Nakai
Roads[edit]
Traffic on Ōme-kaidō heading towards Kabukichō at night
Shuto Expressway:
No.4 Shinjuku Route (Miyakezaka JCT - Takaido)
No.5 Ikebukuro Route (Takebashi JCT - Bijogi JCT)
National highways:
National Route 20 (Shinjuku-dōri, Kōshū-kaidō)
Other major routes:
Tokyo Metropolitan Route 8 (Mejiro-dōri, Shin-Mejiro-dōri)
Tokyo Metropolitan Route 302 (Yasukuni-dōri, Ōme-kaidō)
Tokyo Metropolitan Route 305 (Meiji-dōri)
Education[edit]
Colleges and universities[edit]
Chuo University graduate school
Gakushuin Women's College
Japan Electronics College
Keio University Medical College
Kogakuin University
Lakeland College Japan
Mejiro University
Seibo College
Tokyo Fuji University
Tokyo Medical University
Tokyo University of Science
Tokyo Women's Medical University
Waseda University
Schools[edit]
Public elementary and junior high schools in Shinjuku are operated by the Shinjuku City Board of Education. Public high schools are operated by the Tokyo Metropolitan Government Board of Education.
Koishikawa Technical High School
Ichigaya Commercial High School
Shinjuku High School
Shinjuku Yamabuki High School
Toyama High School
Public institutions[edit]
Tokyo Metropolitan Government Building
Libraries[edit]
Shinjuku operates several public libraries, including the Central Library (with the Children's Library), the Yotsuya Library, the Tsurumaki Library, Tsunohazu Library, the Nishi-Ochiai Library, the Toyama Library, the Kita-Shinjuku Library, the Okubo Library, and the Nakamachi Library. In addition there is a branch library, Branch Library of Central Library in the City Office, located in the city office.[22]
Hospitals[edit]
There are several major hospitals located within the city limits.
Keio University Hospital
International Medical Center of Japan
Social Insurance Chūō General Hospital
Tokyo Medical University Hospital
Tokyo Women's Medical University Hospital
Tokyo Metropolitan Health and Medical Treatment Corporation Ohkubo Hospital
Cultural centers[edit]
Museums[edit]
National Printing Bureau Banknote and Postage Stamp Museum
National Museum of Nature and Science, Shinjuku Branch
Shinjuku Historical Museum
Tokyo Fire Department Museum
Halls[edit]
Tokyo Opera City
Shinjuku Bunka Center
WelCity Tokyo
Meiji Yasuda Life Hall
Shinjuku Koma Theater
Sister cities[edit]
Shinjuku has sister city agreements with several localities:[23]
Lefkada, Greece
Mitte, Berlin, Germany
Dongcheng District, Beijing, China
See also[edit]
Tourism in Japan
References[edit]
^ Shinjuku City
^ Shinjuku City
^ Tokyo Special Wards Map
^ japanvisitor.com
^ http://www.city.shinjuku.lg.jp/content/000021207.pdf
^ History of Shinjuku
^ Ichikawa, 2003
^ 1964 Summer Olympics official report. Volume 2. Part 1. p. 74.
^ "Head Office & Japanese Facilities." Seiko Epson. Retrieved on January 13, 2009.
^ "会社概要." Yoshinoya. Retrieved on February 25, 2010.
^ "Company Info." H.I.S. Retrieved on March 11, 2010.
^ "[1]." Fuji Heavy Industries and Subaru.
^ "Corporate Data." Taisei Corporation. Retrieved on February 20, 2012. "Head Office 1-25-1, Nishi-Shinjuku, Shinjuku-ku, Tokyo 163-0606"
^ "Key Facts." Nihon Kohden. Retrieved on August 9, 2015.
^ "Home." Enoki Films. Retrieved on March 23, 2014. "Enoki Bldg., No. 2, 1-30-10 Shinjuku, Shinjuku-ku, Tokyo 160-0022 Japan"
^ "Headquarter." Jorduan. Retrieved on January 7, 2011. "ZIP 160-0022 2-1-9 Shinjuku, Shinjuku-ku, Tokyo, Japan" (map)
^ "Company Profile." Nissin Foods. Retrieved on August 15, 2009.
^ "会社概要." Airtransse. Retrieved on May 20, 2009.
^ "会社情報." Shinchosha. Retrieved on June 17, 2011. "〒162-8711 東京都新宿区矢来町71"
^ "会社概要." Futabasha. Retrieved on January 7, 2011. "所在地 〒162-8540　東京都新宿区東五軒町3-28" (GIF map of location) (PDF of location)
^ Tokyo Metropolitan Government, governor's office: About Tokyo's prefectural capital (Japanese)
^ http://www.city.shinjuku.tokyo.jp/foreign/english/guide/shisetsu/shisetsu_2.html
^ Friendship cities
Shinjuku Ward Office, History of Shinjuku
Hiroo Ichikawa "Reconstructing Tokyo: The Attempt to Transform a Metropolis" in C. Hein, J.M. Diefendorf, and I. Yorifusa (Eds.) (2003). Building Urban Japan after 1945. New York: Palgrave.
External links[edit]
Shinjuku City official website (Japanese)
Shinjuku City official website (English)
Shinjuku travel guide from Wikivoyage (English)
Shinjuku Architecture and Map
Retrieved from "https://en.wikipedia.org/w/index.php?title=Shinjuku&oldid=709147621"
Categories: 1964 Summer Olympic venuesOlympic athletics venuesWards of TokyoShinjukuHidden categories: Articles with Japanese-language external linksArticles needing additional references from January 2010All articles needing additional referencesArticles containing Japanese-language textCoordinates on WikidataCommons category with page title same as on WikidataWikipedia articles with VIAF identifiers