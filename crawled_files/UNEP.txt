United Nations Environment Programme - Wikipedia, the free encyclopedia
United Nations Environment Programme
(Redirected from UNEP)
The United Nations Environment Programme (UNEP) is an agency that coordinates its environmental activities, assisting developing countries in implementing environmentally sound policies and practices. It was founded by Maurice Strong, its first director, as a result of the United Nations Conference on the Human Environment in June 1972 and has its headquarters in the Gigiri neighborhood of Nairobi, Kenya. UNEP also has six regional offices and various country offices.
Its activities cover a wide range of issues regarding the atmosphere, marine and terrestrial ecosystems, environmental governance and green economy. It has played a significant role in developing international environmental conventions, promoting environmental science and information and illustrating the way those can be implemented in conjunction with policy, working on the development and implementation of policy with national governments, regional institutions in conjunction with environmental non-governmental organizations (NGOs). UNEP has also been active in funding and implementing environment related development projects.
The winner of the Miss Earth beauty pageant serves as the spokesperson of UNEP.[citation needed]
UNEP has aided in the formulation of guidelines and treaties on issues such as the international trade in potentially harmful chemicals, transboundary air pollution, and contamination of international waterways.
The World Meteorological Organization and UNEP established the Intergovernmental Panel on Climate Change (IPCC) in 1988. UNEP is also one of several Implementing Agencies for the Global Environment Facility (GEF) and the Multilateral Fund for the Implementation of the Montreal Protocol, and it is also a member of the United Nations Development Group.[1] The International Cyanide Management Code, a program of best practice for the chemical’s use at gold mining operations, was developed under UNEP’s aegis.
Contents
1 Executive Director
2 Structure
3 International years
4 Reports
5 Reform
6 Main activities
7 Notable world projects
7.1 Glaciers shrinking
7.2 Electric vehicles
8 See also
9 References
10 Further reading
Executive Director[edit]
UNEP's current Executive Director Achim Steiner succeeded previous director Klaus Töpfer in 2006. Dr Töpfer served two consecutive terms, beginning in February 1998.
On 15 March 2006, the former Secretary-General of the United Nations, Kofi Annan, nominated Achim Steiner, former Director General of the IUCN to the position of Executive Director. The UN General Assembly followed Annan's proposal and elected him.
The position was held for 17 years (1975–1992) by Dr. Mostaza cipote Kamal Tolba, who was instrumental in bringing environmental considerations to the forefront of global thinking and action. Under his leadership, UNEP's most widely acclaimed success—the historic 1987 agreement to protect the ozone layer—the Montreal Protocol was negotiated.
During December 1972, the UN General Assembly unanimously elected Maurice Strong to head UNEP. Also Secretary General of both the 1972 United Nations Conference on the Human Environment, which launched the world environment movement, and the 1992 Earth Summit, Strong has played a critical role is globalizing the environmental movement.
Structure[edit]
UNEP's structure includes seven substantive Divisions:
Early Warning and Assessment (DEWA)
Environmental Policy Implementation (DEPI)
Technology, Industry and Economics (DTIE)
Regional Cooperation (DRC)
Environmental Law and Conventions (DELC)
Communications and Public Information (DCPI)
Global Environment Facility Coordination (DGEF)
[3]
International years[edit]
The year 2007 was declared (International) Year of the Dolphin by the United Nations and UNEP.
(International) Patron of the Year of the Dolphin was H.S.H. Prince Albert II of Monaco, with Special Ambassador to the cause being Nick Carter, of the Backstreet Boys.[4]
2010 was designated the International Year of Biodiversity and presented an opportunity to enhance knowledge of ecosystems and their services.
In 2011 the UN celebrated the International Year of Forests.
In 2012, the International Year for Sustainable Energy for All.
2013 has been designated as the International Year of Water Cooperation.
(See international observance and list of environmental dates.)
Reports[edit]
UNEP publishes many reports, atlases and newsletters. For instance, the fifth Global Environment Outlook (GEO-5) assessment is a comprehensive report on environment, development and human well-being, providing analysis and information for policy makers and the concerned public. One of many points in the GEO-5 warns that we are living far beyond our means. It notes that the human population is now so large that the amount of resources needed to sustain it exceeds what is available.
In June 2010, a report from UNEP declared that a global shift towards a vegan diet was needed to save the world from hunger, fuel shortages and climate change.[5]
Reform[edit]
Main articles: International Sustainable Energy Agency, UNEO and IRENA
Following the publication of Fourth Assessment Report of the Intergovernmental Panel on Climate Change (IPCC) in February 2007, a "Paris Call for Action" read out by French President Jacques Chirac and supported by 46 countries, called for the United Nations Environment Programme to be replaced by a new and more powerful "United Nations Environment Organization (UNEO)", also called Global Environment Organisation now supported by French President Nicolas Sarkozy and German Chancellor Angela Merkel, to be modelled on the World Health Organization. The 46 countries included the European Union nations, but notably did not include the United States, Saudi Arabia, Russia, and China, the top four emitters of greenhouse gases.[6]
In December 2012, following the Rio+20 Summit, a decision by the General Assembly of the United Nations to 'strengthen and upgrade' the UN Environment Programme (UNEP) and establish universal membership of its governing body was confirmed.
Main activities[edit]
UNEP's main activities are related to:[7]
climate change;
including the Territorial Approach to Climate Change (TACC);
disasters and conflicts;
ecosystem management;
environmental governance;
environment under review;
harmful substances; and
resource efficiency.
Notable world projects[edit]
UNEP has sponsored the development of solar loan programs, with attractive return rates, to buffer the initial deployment costs and entice consumers to consider and purchase solar PV systems. The most famous example is the solar loan program sponsored by UNEP helped 100,000 people finance solar power systems in India.[8] Success in India's solar program has led to similar projects in other parts of the developing world like Tunisia, Morocco, Indonesia and Mexico.
UNEP sponsors the Marshlands project in the Middle East . In 2001, UNEP alerted the international community to the destruction of the Marshlands when it released satellite images showing that 90 percent of the Marshlands had already been lost. The UNEP "support for Environmental Management of the Iraqi Marshland" commenced in August 2004, in order to manage the Marshland area in an environmentally sound manner.[9]
In order to ensure full participation of global communities, UNEP works in an inclusive fashion that brings on board different societal cohorts. UNEP has a vibrant programme for young people known as Tunza. Within this program are other projects like the AEO for Youth.[10]
Glaciers shrinking[edit]
Glaciers are shrinking at record rates and many could disappear within decades, the U.N. Environment Programme said on March 16, 2008. The scientists measuring the health of almost 30 glaciers around the world found that ice loss reached record levels in 2006. On average, the glaciers shrank by 4.9 feet in 2006, the most recent year for which data are available. The most severe loss was recorded at Norway's Breidalblikkbrea glacier, which shrank 10.2 feet in 2006. Glaciers lost an average of about a foot of ice a year between 1980 and 1999. But since the turn of the millennium the average loss has increased to about 20 inches.[11]
Electric vehicles[edit]
At the fifth Magdeburg Environmental Forum held from 3–4 July 2008, in Magdeburg, Germany, UNEP and car manufacturer Daimler called for the establishment of infrastructure for electric vehicles. At this international conference, 250 high-ranking representatives from ce, politics and non-government organizations discussed solutions for future road transportation under the motto of "Sustainable Mobility–the Post-2012 CO2 Agenda".[12]
See also[edit]
2010 Biodiversity Indicators Partnership
United Nations Environment Program Finance Initiative
Global warming
International Renewable Energy Agency
Melbourne Principles
Miss Earth Foundation
Timeline of environmental events
UNEP GEO Data Portal
UNEP/GRID-Arendal
United Nations Billion Tree Campaign
World Conservation Monitoring Centre
References[edit]
^ http://www.undg.org/index.cfm?P=13
^ Schrijver, Nico (2010). Development Without Destruction: The UN and Global Resource Management. United Nations Intellectual History Project Series. Bloomington, IN: Indiana University Press. p. 116. ISBN 978-0-253-22197-1.
^ "UNEP Offices". UNEP.
^ Newswise Science News | Pop Superstar Nick Carter to Help Wild Dolphins and Oceans
^ Felicity Carus UN urges global move to meat and dairy-free diet, The Guardian, 2 June 2010
Also see "Energy and Agriculture Top Resource Panel's Priority List for Sustainable 21st Century", United Nations Environment Programme (UNEP), Brussels, 2 June 2010.
^ Doyle, Alister (2007-02-03). "46 nations call for tougher U.N. environment role". Reuters.
^ "United Nations Environment Programme". unep.org. November 2011. Retrieved November 17, 2011.
^ Solar loan program in India
^ UNEP Marshland project in Middle East
^ AEO-for-Youth
^ U.N.: Glaciers shrinking at record rate
^ "UNEP and Daimler Call for Infrastructure for Electric and Fuel-cell Vehicles". Climate-L.org. 4 July 2008. Retrieved June 16, 2010.
Further reading[edit]
United Nations Environment Programme. "Natural Allies: UNEP and Civil Society." Nairobi: United Nations Foundation, 2004.
Paul Berthoud, A Professional Life Narrative, 2008, worked with UNEP and offers testimony from the inside of the early years of the organization.
Helpful links
United Nations Environment Programme
UNEP Finance Initiative
Frankfurt School – UNEP Collaborating Centre for Climate & Sustainable Energy Finance
United Nations Environment Programme - World Conservation Monitoring Centre (UNEP-WCMC)
UNEP-Tongji Institute of Environment for Sustainable Development
UNEP/GRID-Europe
UNEP GEO Data Portal
Sindrom Kodok Pada Manusia (Indonesia Language)
Netherlands Commission for Environmental Assessment
UNEP Regional Seas Programme
Resources on United Nations Environment Programme (UNEP
Retrieved from "https://en.wikipedia.org/w/index.php?title=United_Nations_Environment_Programme&oldid=706754676"
Categories: United Nations Environment ProgrammeGlobal environmental organizationsUnited Nations Development GroupEnvironmental organizations established in 19721972 in the environmentHidden categories: All articles with unsourced statementsArticles with unsourced statements from December 2015Wikipedia articles with VIAF identifiersWikipedia articles with LCCN identifiersWikipedia articles with ISNI identifiersWikipedia articles with GND identifiersWikipedia articles with SELIBR identifiersWikipedia articles with BNF identifiersWikipedia articles with BIBSYS identifiersWikipedia articles with ULAN identifiersWikipedia articles with NLA identifiers