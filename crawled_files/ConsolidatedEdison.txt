Consolidated Edison - Wikipedia, the free encyclopedia
Consolidated Edison
"ConEd" redirects here. It is not to be confused with ComEd (Commonwealth Edison) in Illinois.
Con Ed plant on the East River at 15th Street in Manhattan, New York City. Following the September 11 attacks, the 15th Street exit from the FDR Drive, announced by the sign on the right, was permanently closed.
Consolidated Edison, Inc., commonly known as Con Edison or Con Ed, is one of the largest investor-owned energy companies in the United States, with approximately $13 billion in annual revenues as of 2010, and over $36 billion in assets. The company provides a wide range of energy-related products and services to its customers through its subsidiaries:
Consolidated Edison Company of New York, Inc., (CECONY), a regulated utility providing electric, gas, and steam service in New York City and Westchester County, New York;
Orange and Rockland Utilities, Inc., a regulated utility serving customers in a 1,350-square-mile (3,500 km2) area in southeastern New York and adjacent sections of northern New Jersey and northeastern Pennsylvania;
Con Edison Solutions, a retail energy supply and services company;
Con Edison Energy, a wholesale energy supply company; and
Con Edison Development, a company that owns and operates generating plants and participates in other infrastructure projects.
In 2014, electric revenues accounted for 70.54% of consolidated sales (70.84 in 2013); gas revenues 14.96% (14.74%); steam revenues 4.86% (5.52%); and non-utility revenues 9.62% (8.85%).[1] Though the company provides an indispensable service to New York residents, a number of major incidents and service problems have negatively impacted its reputation with the public.
Contents
1 History
2 Systems
2.1 Electrical
2.2 Gas
2.3 Steam
3 Headquarters
4 Leadership and associations
5 Major accidents and incidents
6 Honors and criticism
7 Adaptive re-use of former Con Ed building
8 See also
9 References
10 External links
History[edit]
A sketch of an early power plant on Pearl Street
As well as gas and electricity, Con Ed supplies steam to New York City
In 1823, Con Edison’s earliest corporate predecessor, the New York Gas Light Company, was founded by a consortium of New York City investors. A year later, it was listed on the New York Stock Exchange. In 1884, six gas companies combined into the Consolidated Gas Company.
The New York Steam Company began providing service in lower Manhattan in 1882. Today, Con Edison operates the largest commercial steam system in the world, providing steam service to nearly 1,600 commercial and residential establishments in Manhattan from Battery Park to 96th Street.[2]
Con Edison’s electric business also dates back to 1882, when Thomas Edison’s Edison Illuminating Company of New York began supplying electricity to 59 customers in a square-mile area in lower Manhattan. After the “War of Currents”, there were more than 30 companies generating and distributing electricity in New York City and Westchester County. But by 1920 there were far fewer, and the New York Edison Company (then part of Consolidated Gas) was clearly the leader.
In 1936, with electric sales far outstripping gas sales, the company incorporated and the name was changed to Consolidated Edison Company of New York, Inc. The years that followed brought further amalgamations as Consolidated Edison acquired or merged with more than a dozen companies between 1936 and 1960. Con Edison today is the result of acquisitions, dissolutions and mergers of more than 170 individual electric, gas and steam companies.
On January 1, 1998, following the deregulation of the utility industry in New York state, a holding company, Consolidated Edison, Inc., was formed. It is one of the nation’s largest investor-owned energy companies, with approximately $14 billion in annual revenues and $33 billion in assets. The company provides a wide range of energy-related products and services to its customers through two regulated utility subsidiaries and three competitive energy businesses. Under a number of corporate names, the company has been traded on the NYSE without interruption since 1824—longer than any other NYSE stock. Its largest subsidiary, Consolidated Edison Company of New York, Inc. provides electric, gas and steam service to more than 3 million customers in New York City and Westchester County, New York, an area of 660 square miles (1,700 km2) with a population of nearly 9 million.
Systems[edit]
Electrical[edit]
The Con Edison electrical transmission system utilizes voltages of 138 kilovolts (kV), 345 kV, and 500 kV. The company has two 345 kV interconnections with upstate New York that enable it to import power from Hydro-Québec in Canada and one 345 kV interconnection each with Public Service Electric and Gas in New Jersey and LIPA on Long Island. Con Edison is also interconnected with Public Service Electric and Gas via the Branchburg-Ramapo 500 kV line. Con Ed's distribution voltages are 33 kV, 27 kV, 13 kV, and 4 kV.
The 93,000 miles (150,000 km) of underground cable in the Con Edison system could wrap around the Earth 3.6 times. Nearly 36,000 miles (58,000 km) of overhead electric wires complement the underground system—enough cable to stretch between New York and Los Angeles 13 times.[3]
Gas[edit]
The Con Edison gas system has nearly 7,200 miles (11,600 km) of pipes—if laid end to end, long enough to reach Paris and back to New York City, and serves Westchester County, the Bronx, Manhattan and parts of Queens. Gas service in the other boroughs is provided by National Grid USA. The average volume of gas that travels through Con Edison’s gas system annually could fill the Empire State Building nearly 6,100 times.[4]
Steam[edit]
Main article: New York City steam system
Con Edison produces 30 billion pounds of steam each year through its seven power plants which boil water to 1,000 °F (538 °C) before pumping it to hundreds of buildings in the New York City steam system, which is the biggest district steam system in the world.[5] Steam traveling through the system is used to heat and cool some of New York’s most famous addresses, including the United Nations complex, the Empire State Building, and the Metropolitan Museum of Art.[6]
Headquarters[edit]
The tower of the company's headquarters, the Consolidated Edison Company Building on Irving Place and 14th Street in Manhattan, with its landmark clock, as seen from 14th Street. Part of the Zeckendorf Towers is in the foreground.
The "Tower of Light" at the top of the building's tower,[7] seen from Third Avenue
The Consolidated Edison Company Building, designed by Henry J. Hardenbergh, was built between 1910 and 1914. The building, at 4 Irving Place, takes up the entire block between East 14th and 15th Streets and Irving Place and Third Avenue, and was originally built for the Consolidated Gas Company, although its predecessor companies, such as the Manhattan Gas Light Company, were located at the same address as early as 1854. The new building's location had been the site of the Academy of Music, New York's third opera house,[7][8] as well as the original Tammany Hall building.[9]
Warren & Wetmore's 26-story tower—topped by a "Tower of Light" designed to look like a miniature temple and capped by a bronze lantern which lights up at night[7]—was added between 1926 and 1929.
Leadership and associations[edit]
John Mc Avoy, Chairman and Chief Executive Officer
Craig S. Ivey, President
Robert Hoglund, Senior Vice President and Chief Financial Officer
Carole Sobin, Secretary
Robert Muccilo, Vice President, Controller and Chief Accounting Officer
Scott L. Sanders, Vice President and Treasurer
Elizabeth D. Moore, General Counsel
ConEd Solutions is a member of Real Estate Board of New York.[10]
Major accidents and incidents[edit]
1989: A steam pipe explosion in Gramercy Park killed three, injured 24, and required the evacuation of a damaged apartment building due to high levels of asbestos in the air. Workers had failed to drain water from the pipe before turning the steam on. The utility also eventually pleaded guilty to lying about the absence of asbestos contamination, and paid a $2 million fine.[11]
2004: In Manhattan, stray voltage killed a woman walking her dog in the East Village when she stepped on an electrified metal plate.[12]
2006: After the blackout in Queens, the company was criticized by public officials for a poor record in the restoration of service to its customers.[13]
2007: On July 18, an explosion occurred in midtown Manhattan near Grand Central Terminal when an 83-year-old Con Edison steam pipe failed, resulting in one death, over 40 injuries, as well as subway and surface disruptions.[14]
2007: The day before Thanksgiving, an explosion critically burned Queens resident Kunta Oza when an 80-year-old cast iron gas main ruptured. Oza died on Thanksgiving Day, and her family later settled with Con Edison for $3.75 million.[15]
2009: Another gas explosion claimed a life in Queens while Con Edison personnel were on the scene. There was a leak in a manhole and a fault in an electrical feeder at the same time. The fault in the feeder caused the explosion due to the sparks being generated. When the mechanic opened the manhole more oxygen entered and the explosion took place.[citation needed] Due to that event Con Edison has changed its procedure on outside gas leak calls.[16]
2012:
On October 29, flooding from Hurricane Sandy caused a transformer explosion at a Con-Ed plant on New York City's East Side.[17][18]
During the storm, Con Edison used social media to get outage and restoration information out to customers. The company’s Twitter account gained an extra 16,000 followers during the storm.[19][20]
Con Edison's subsidiary, Orange & Rockland Utilities, was criticized for its response to Hurricane Sandy. Some customers experienced a loss of electrical power for 11 days.[21]
2014: On March 12, two apartment buildings exploded in East Harlem after a reported Con Edison gas leak. Eight people were killed in the massive explosion that reduced the conjoining buildings to rubble.[22][23]
Honors and criticism[edit]
In March 2002, Fortune magazine named the company as one of "America's Most Admired Companies" in the publication's newest corporate ranking survey. In 2003, Con Edison ranked second on the top ten list for electric and gas utilities.[24]
In December 2011, the non-partisan organization Public Campaign released a report criticizing ConEd for spending $1.8 million on lobbying and not paying any taxes during 2008–2010, instead getting $127 million in tax rebates, despite making a profit of $4.2 billion, and increasing executive pay by 82% to $17.4 million in 2010 for its top five executives.[25]
In 2014, Con Edison was named one of the 50 best companies for Latinas by Latina Style Magazine.[26] In its "Best of the Best" issue in 2015, Hispanic Network Magazine named the company a top employer among energy, gas, and oil companies.[27] Con Edison was also selected as a top regional utility by DiversityInc magazine.[28]
Adaptive re-use of former Con Ed building[edit]
Main article: Avatar Studios
A former Con Edison building on West 53rd Street in Manhattan was converted first into the studio for the television game show Let's Make a Deal, and later into a recording studio called “The Power Station” because of its Edison history. In 1996, the studio was renamed Avatar Studios.
See also[edit]
References[edit]
^ Con Edison Investor Relations. [1] "Con Edison" (February 19, 2015)
^ "'A Tale of Two Cities – New York' – The New York City Steam System". International District Energy Association. Retrieved 2008-01-09. With district steam service commencing in 1882, Con Edison owns and operates the largest downtown steam system in the world, serving over 1600 buildings with steam supplied from multiple combined heat and power facilities with total capacity of 21,755 (Mlbs/hr) and 627 MW. In 1999, Con Ed completed the ten-year Steam Enhancement program investing over $200 million in system upgrades and maintenance.
^ "Electric System". Con Edison. Retrieved 2008-01-09. Con Edison operates one of the most complex electric power systems in the world. It is also the world's most reliable.
^ "Gas System". Con Edison. Retrieved 2008-01-09. Con Edison distributes natural gas to 1.1 million customers in Manhattan, the Bronx, Queens, and Westchester County, making us one of the larger gas distribution companies in the United States.
^ Bevelhymer, Carl. "Steam". Gotham Gazette. Retrieved 2008-01-09. When John Velez, co-owner of Sutton Cleaners, arrives at work at 7 a.m. on Manhattan's East Side, he opens a steam valve in the back of his shop. 'When I come into the shop in the morning, it's one, two, three,' he says, 'and you're up and running in less than a minute.'
^ "Steam System". Con Edison. The New York Steam Company began providing service in lower Manhattan in 1882. Today, Con Edison operates the largest steam system in the world. The system contains approximately 105 miles (169 km) of mains and service pipes and 3,000 steam manholes. Steam is provided from seven Con Edison steam-generating plants, five in Manhattan, one in Queens, and one in Brooklyn, along with receiving steam under contract from a steam plant at the Brooklyn Navy Yard.
^ a b c Mendelsohn, Joyce. Touring the Flatiron. New York: New York Landmarks Conservancy, 1998. ISBN 0-964-7061-2-1
^ The Academy of Music was preceded by the Italian Opera House of the short-lived New York Opera Company (1833–1835), the first American building designed specifically for opera; and the Astor Opera House (opened 1847) which closed soon after an infamous riot on May 10, 1849 sparked by competing performances of Macbeth by William Charles Macready and Edwin Forrest. Burrows, Edwin G. & Wallace, Mike (1999). Gotham: A History of New York City to 1898. New York: Oxford University Press. ISBN 0195116348.  p. 585, pp. 761–765
^ Wurman, Richard Saul, Access New York City. New York: HarperCollins, 2000. ISBN 0-06-277274-0
^ http://www.rebny.com/content/rebny/en/directory/member-firms.html
^ Pitt, David E (1989-08-24). "Evacuation For Asbestos Near Blast Site". The New York Times. Retrieved 2008-01-09. More than 200 residents of a Gramercy Park apartment building that was heavily damaged in a steam-pipe explosion over the weekend were ordered from their homes last night after tests showed what a Consolidated Edison official called "extremely high" levels of asbestos fibers throughout the building.
^ Chan, Sewell (2006-03-04). "Con Ed Finds 1,214 Stray Voltage Sites in One Year". The New York Times. Retrieved 2008-01-09. Consolidated Edison, responding to testing requirements imposed after a woman was electrocuted while walking her dog in the East Village in 2004, found 1,214 instances of stray voltage during a yearlong examination of electrical equipment on city streets, officials disclosed at a City Council hearing yesterday.
^ Chan, Sewell. "Con Edison Is Ordered to Return $18 Million to Customers" New York Times (November 7, 2007)
^ "STEAM REPORT: BUBBLE COLLAPSE WATERHAMMER CAUSED LEXINGTON AVENUE INCIDENT". Con Edison. Retrieved 2008-01-09. The steam pipe rupture at Lexington Avenue and East 41 Street on July 18 was caused by a bubble-collapse water hammer that generated a momentary force against the pipe's wall that was more than seven times greater than the pipe's normal operating pressure, according to reports issued today by two independent experts commissioned by Con Edison. The pipe itself was found to be in good condition and did not contribute to the event.
^ "Anger over gas explosion death". Daily News. New York. 2007-12-20.
^ Wilson, Michael (2009-04-26). "House Exploded Just Before a Check, Con Ed Says". The New York Times. Retrieved 2010-04-30.
^ http://www.youtube.com/watch?v=ZAqYZ433TeQ
^ "Massive Con Ed Transformer Explosion Blamed For Widespread Outage". CBS. October 30, 2012. Retrieved October 31, 2012.
^ Gabbatt, Adam. "How companies used social media during Hurricane Sandy" The Guardian (February 20, 2013)
^ Anderson, Jared. "How Con Edison Effectively Relied on Social Media to Reach Customers During Superstorm Sandy" Breaking Energy (July 30, 2014)
^ Sullivan, S. P. "State hearing in Ramsey will focus on Orange & Rockland's Hurricane Sandy response" NJ.com (December 7, 2012)
^ Santora, Marc "At Least 3 Killed as Gas Explosion Hits East Harlem" New York Times (March 12, 2014)
^ Santora, Marc and Rashbaum, William K. "Rescue Effort in East Harlem Yields Only More Victims". New York Times. Retrieved 13 March 2014.
^ "Con Edison One of America's Most Admired Companies".
^ Portero, Ashley. "30 Major U.S. Corporations Paid More to Lobby Congress Than Income Taxes, 2008–2010". International Business Times. Archived from the original on 26 December 2011. Retrieved 26 December 2011.
^ http://latinastyle.com/latina-style-inc-announces-the-2014-latina-style-50-report/
^ http://www.hnmagazine.com/article/hispanic-network-magazine-announces-its-spring-2015-early-results-best-best-lists
^ http://www.diversityinc.com/top-7-utilities/
External links[edit]
Official website
Con Edison, Inc. (holding company)
Retrieved from "https://en.wikipedia.org/w/index.php?title=Consolidated_Edison&oldid=694086392"
Categories: Companies listed on the New York Stock ExchangeCompanies based in New York CityThomas EdisonPower companies of the United StatesCompanies established in 1823Companies in the Dow Jones Utility AverageConsolidated EdisonHidden categories: Pages using infobox company with unsupported parametersAll articles with unsourced statementsArticles with unsourced statements from March 2011