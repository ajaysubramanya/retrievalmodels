Absorption refrigerator - Wikipedia, the free encyclopedia
Absorption refrigerator
(Redirected from Absorption chiller)
An absorption refrigerator is a refrigerator that uses a heat source (e.g., solar energy, a fossil-fueled flame, waste heat from factories, or district heating systems) which provides the energy needed to drive the cooling process.
Absorption refrigerators are often used for food storage in recreational vehicles. The principle can also be used to air-condition buildings using the waste heat from a gas turbine or water heater. Using waste heat from a gas turbine makes the turbine very efficient because it first produces electricity, then hot water, and finally, air-conditioning (called cogeneration/trigeneration).
The standard for the absorption refrigerator is given by the ANSI/AHRI standard 560-2000.[1]
Contents
1 History
2 Principles
2.1 Simple salt and water system
2.2 Water spray absorption refrigeration
2.3 Single pressure absorption refrigeration
3 References
4 See also
5 Further reading
6 External links
History[edit]
In the early years of the twentieth century, the vapor absorption cycle using water-ammonia systems was popular and widely used, but after the development of the vapor compression cycle it lost much of its importance because of its low coefficient of performance (about one fifth of that of the vapor compression cycle). Nowadays, the vapor absorption cycle is used only where waste heat is available or where heat is derived from solar collectors. Absorption refrigerators are a popular alternative to regular compressor refrigerators where electricity is unreliable, costly, or unavailable, where noise from the compressor is problematic, or where surplus heat is available (e.g., from turbine exhausts or industrial processes, or from solar plants).
Absorption cooling was invented by the French scientist Ferdinand Carré in 1858.[2] The original design used water and sulphuric acid.
In 1922 Baltzar von Platen and Carl Munters, while they were still students at the Royal Institute of Technology in Stockholm, Sweden, enhanced the principle with a 3-fluid configuration. This "Platen-Munters" design can operate without a pump.
Commercial production began in 1923 by the newly formed company AB Arctic, which was bought by Electrolux in 1925. In the 1960s, the absorption refrigeration saw a renaissance due to the substantial demand for refrigerators for caravans. AB Electrolux established a subsidiary in the United States, named Dometic Sales Corporation. The company marketed refrigerators for RVs under the Dometic brand. In 2001, Electrolux sold most of its leisure products line to the venture-capital company EQT which created Dometic as a stand-alone company.
In 1926, Albert Einstein and his former student Leó Szilárd proposed an alternative design known as the Einstein refrigerator.[3]
At the 2007 TED Conference, Adam Grosser presented his research of a new, very small, "intermittent absorption" vaccine refrigeration unit for use in third world countries. The refrigerator is a small unit placed over a campfire, that can later be used to cool 15 liters of water to just above freezing for 24 hours in a 30 °C environment.[4]
Principles[edit]
Absorption cooling process
Both absorption and compressor refrigerators use a refrigerant with a very low boiling point (less than 0 °F (−18 °C)). In both types, when this refrigerant evaporates (boils), it takes some heat away with it, providing the cooling effect. The main difference between the two systems is the way the refrigerant is changed from a gas back into a liquid so that the cycle can repeat. An absorption refrigerator changes the gas back into a liquid using a method that needs only heat, and has no moving parts other than the refrigerant itself.
The absorption cooling cycle can be described in three phases:
Evaporation: A liquid refrigerant evaporates in a low partial pressure environment, thus extracting heat from its surroundings (e.g. the refrigerator's compartment). Because of the low partial pressure, the temperature needed for evaporation is also low.
Absorption: The now gaseous refrigerant is absorbed by another liquid (e.g. a salt solution).
Regeneration: The refrigerant-saturated liquid is heated, causing the refrigerant to evaporate out. The hot gaseous refrigerant passes through a heat exchanger, transferring its heat outside the system (such as to surrounding ambient-temperature air), and condenses. The condensed (liquid) refrigerant supplies the evaporation phase.
In comparison, a compressor refrigerator uses an electrically powered compressor to increase the pressure on the gaseous refrigerant, the resulting hot high pressure gas is condensed to a liquid form by cooling in a heat exchanger with the external environment (usually air in the room). The condensed refrigerant now at a temperature near to that of the external environment, then passes through an orifice or a throttle valve into the evaporator section. The orifice or throttle valve creates a pressure drop between the high pressure condenser section and the low pressure evaporator section. The lower pressure in the evaporator section allows the liquid refrigerant to evaporate more easily and in the process of evaporating, absorb heat from the refrigerator food compartment. The vaporized refrigerant then goes back into the compressor to repeat the cycle.
Another difference between the two types is the refrigerant used. Compressor refrigerators typically use an HCFC or HFC, while absorption refrigerators typically use ammonia or water.
Simple salt and water system[edit]
A simple absorption refrigeration system common in large commercial plants uses a solution of lithium bromide salt and water. Water under low pressure is evaporated from the coils that are being chilled. The water is absorbed by a lithium bromide/water solution. The system drives the water off the lithium bromide solution with heat.[5]
Water spray absorption refrigeration[edit]
Water spray absorption system
Another variant, depicted to the right, uses air, water, and a salt water solution. The intake of warm, moist air is passed through a sprayed solution of salt water. The spray lowers the humidity but does not significantly change the temperature. The less humid, warm air is then passed through an evaporative cooler, consisting of a spray of fresh water, which cools and re-humidifies the air. Humidity is removed from the cooled air with another spray of salt solution, providing the outlet of cool, dry air.
The salt solution is regenerated by heating it under low pressure, causing water to evaporate. The water evaporated from the salt solution is re-condensed, and rerouted back to the evaporative cooler.
Single pressure absorption refrigeration[edit]
Labeled photo of a domestic absorption refrigerator. 1. Hydrogen enters the pipe with liquid ammonia (or lithium bromide solution)
2. Ammonia and hydrogen enter the inner compartment of the refrigerator. An increase in volume causes a decrease in the partial pressure of the liquid ammonia. The ammonia evaporates, taking heat from the liquid ammonia (ΔHVap) and thus lowering its temperature. Heat flows from the hotter interior of the refrigerator to the colder liquid, promoting further evaporation.
3. Ammonia and hydrogen return from the inner compartment, ammonia returns to absorber and dissolves in water. Hydrogen is free to rise upwards.
4. Ammonia gas condensation (passive cooling).
5. Hot ammonia (gas).
6. Heat insulation and distillation of ammonia gas from water.
7. Heat source (electric).
8. Absorber vessel (water and ammonia solution).
A single-pressure absorption refrigerator takes advantage of the fact that a substance's boiling point depends upon the partial pressure of the vapor above the liquid and goes up with higher partial pressure. While having the same total pressure throughout the system, the refrigerator maintains a low partial pressure of the refrigerant (therefore low boiling point) in the part of the system that draws heat out of the low-temperature interior of the refrigerator, but maintains the refrigerant at high partial pressure (therefore high boiling point) in the part of the system that expels heat to the high-temperature air outside the refrigerator.
The refrigerator uses three substances: ammonia, hydrogen gas, and water. The cycle is closed, with all hydrogen, water and ammonia collected and endlessly reused. The system is pressurized to the pressure where the boiling point of ammonia is higher than the temperature of the condenser coil (the coil which transfers heat to the air outside the refrigerator, by being hotter than the outside air) This pressure is typically 14-16atm.
The cooling cycle starts with liquid ammonia entering the evaporator at room temperature. The volume of the evaporator is greater than the volume of the liquid, with the excess space occupied by a mixture of gaseous ammonia and hydrogen. The presence of hydrogen lowers the partial pressure of the ammonia gas, thus lowering the boiling point of the liquid below the temperature of the refrigerator's interior. Ammonia evaporates, taking a small amount of heat from the liquid and lowering the liquid's temperature, until it reaches that boiling point. It then continues to evaporate, without the liquid descending below the boiling point, while the large enthalpy of vaporization (heat) flows from the warmer refrigerator interior to the cooler liquid ammonia and then to more ammonia gas.
In the next two steps, the ammonia gas is separated from the hydrogen to take advantage of the full-pressure ammonia's higher boiling point.
The ammonia (gas) and hydrogen (gas) solution flows through a pipe from the evaporator into the absorber. In the absorber, the solution of gas flows into a solution of ammonia (liquid) and water (liquid). The ammonia dissolves in the water and the hydrogen collects at the top of the absorber, while the ammonia (liquid) and water (liquid) solution remains at the bottom. The hydrogen is now separate, but the ammonia is now mixed with water.
The next step separates the ammonia and water. The ammonia/water solution flows to the generator, where heat is applied to evaporate the ammonia, leaving most of the water, with its higher boiling point, behind. Some water vapor and bubbles remain mixed with the ammonia; this water is removed in the final separation step, by passing it through the separator, an uphill series of twisted pipes with minor obstacles to pop the bubbles, allowing the water vapor to condense and drain back to the generator.
The pure ammonia gas then enters the condenser. In this heat exchanger, the hot ammonia gas transfers its heat to the outside air, which is below the boiling point of the full-pressure ammonia, and therefore condenses. The condensed (liquid) ammonia flows down to be mixed with the hydrogen gas from the absorption step, allowing the cycle to repeat.
References[edit]
^ PDF document for download at http://www.ahrinet.org/App_Content/ahri/files/standards%20pdfs/ANSI%20standards%20pdfs/ANSI%20ARI560-2000.pdf
^ Eric Granryd & Björn Palm, Refrigerating engineering, Stockholm Royal Institute of Technology, 2005, see chap. 4-3
^ "US Patent 1781541".
^ Adam Grosser (Feb 2007). "Adam Grosser and his sustainable fridge, 552 word transcript and video 3mins34". TED. Archived from the original on 19 April 2010. Retrieved 2010-04-18.
^ Sapali, S. N. "Lithium Bromide Absorption Refrigeration System". Textbook Of Refrigeration And Air-Conditioning. New Delhi: PHI learning. p. 258. ISBN 978-81-203-3360-4.
See also[edit]
Quantum absorption refrigerator
RV Fridge
Further reading[edit]
Levy, A.; Kosloff, R. (2012). "Quantum Absorption Refrigerator". Phys. Rev. Lett. 108: 070604. arXiv:1109.0728. Bibcode:2012PhRvL.108g0604L. doi:10.1103/PhysRevLett.108.070604.
External links[edit]
Absorption Heat Pumps (EERE).
Arizona Energy Explanation with diagrams
Design Analysis of the Einstein Refrigeration Cycle, Andrew Delano (1998). Retrieved September 13, 2005.
Air Conditioning Thermodynamics, published by the California EPA, Air Resources Board
Lithium-Bromide / Water Cycle - Absorption Refrigeration for Campus Cooling at BYU.
Retrieved from "https://en.wikipedia.org/w/index.php?title=Absorption_refrigerator&oldid=708989649"
Categories: Thermodynamic cyclesHeat pumpsCooling technologyHeating, ventilating, and air conditioningGas technologies