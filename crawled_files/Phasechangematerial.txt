Phase-change material - Wikipedia, the free encyclopedia
Phase-change material
A sodium acetate heating pad. When the sodium acetate solution crystallises, it becomes warm.
A phase-change material (PCM) is a substance with a high heat of fusion which, melting and solidifying at a certain temperature, is capable of storing and releasing large amounts of energy. Heat is absorbed or released when the material changes from solid to liquid and vice versa; thus, PCMs are classified as latent heat storage (LHS) units.
Contents
1 Characteristics and classification
1.1 Organic PCMs
1.2 Inorganic
1.3 Eutectics
1.4 Hygroscopic materials
2 Selection criteria
3 Thermophysical properties
3.1 Common PCMs
3.2 Commercially available PCMs near room temperature
4 Technology, development and encapsulation
5 Thermal composites
6 Applications
7 Fire and safety issues
8 See also
9 References
10 Further reading
Characteristics and classification[edit]
PCMs latent heat storage can be achieved through liquid–solid, solid–liquid, solid–gas and liquid–gas phase change. However, the only phase change used for PCMs is the solid–liquid change. Liquid-gas phase changes are not practical for use as thermal storage due to the large volumes or high pressures required to store the materials when in their gas phase. Liquid–gas transitions do have a higher heat of transformation than solid–liquid transitions. Solid–solid phase changes are typically very slow and have a rather low heat of transformation.
Initially, the solid–liquid PCMs behave like sensible heat storage (SHS) materials; their temperature rises as they absorb heat. Unlike conventional SHS, however, when PCMs reach the temperature at which they change phase (their melting temperature) they absorb large amounts of heat at an almost constant temperature. The PCM continues to absorb heat without a significant rise in temperature until all the material is transformed to the liquid phase. When the ambient temperature around a liquid material falls, the PCM solidifies, releasing its stored latent heat. A large number of PCMs are available in any required temperature range from −5 up to 190 °C.[1] Within the human comfort range between 20–30 °C, some PCMs are very effective. They store 5 to 14 times more heat per unit volume than conventional storage materials such as water, masonry or rock.[2]
Organic PCMs[edit]
Paraffin (CnH2n+2) and fatty acids (CH3(CH2)2nCOOH)[3]
Advantages
Freeze without much undercooling
Ability to melt congruently
Self nucleating properties
Compatibility with conventional material of construction
No segregation
Chemically stable
High heat of fusion
Safe and non-reactive
Recyclable
Disadvantages
Low thermal conductivity in their solid state. High heat transfer rates are required during the freezing cycle
Volumetric latent heat storage capacity is low
Flammable. This can be partially alleviated by specialist containment
To obtain reliable phase change points, most manufacturers use technical grade paraffins which are essentially paraffin mixture(s) and are completely refined of oil, resulting in high costs
Inorganic[edit]
Salt hydrates (MnH2O)[4]
Advantages
High volumetric latent heat storage capacity
Availability and low cost
Sharp melting point
High thermal conductivity
High heat of fusion
Non-flammable
Disadvantages
Change of volume is very high
Super cooling is major problem in solid–liquid transition
Nucleating agents are needed and they often become inoperative after repeated cycling
Eutectics[edit]
c-inorganic, inorganic-inorganic compounds
Advantages
Eutectics have sharp melting point similar to pure substance
Volumetric storage density is slightly above organic compounds
Disadvantages
Only limited data is available on thermo-physical properties as the use of these materials are relatively new to thermal storage application
Hygroscopic materials[edit]
Many natural building materials are hygroscopic, that is they can absorb (water condenses) and release water (water evaporates). The process is thus:
Condensation (gas to liquid) ΔH<0; enthalpy decreases (exothermic process) gives off heat.
Vaporization (liquid to gas) ΔH>0; enthalpy increases (endothermic process) absorbs heat (or cools).
Whilst this process liberates a small quantity of energy, large surfaces area allows significant (1–2 °C) heating or cooling in buildings. The corresponding materials are wool insulation, earth/clay render finishes,.
Selection criteria[edit]
Thermodynamic properties. The phase change material should possess:[5]
Melting temperature in the desired operating temperature range
High latent heat of fusion per unit volume
High specific heat, high density and high thermal conductivity
Small volume changes on phase transformation and small vapor pressure at operating temperatures to reduce the containment problem
Congruent melting
Kinetic properties
High nucleation rate to avoid supercooling of the liquid phase
High rate of crystal growth, so that the system can meet demands of heat recovery from the storage system
Chemical properties
Chemical stability
Complete reversible freeze/melt cycle
No degradation after a large number of freeze/melt cycle
Non-corrosiveness, non-toxic, non-flammable and non-explosive materials
Economic properties
Low cost
Availability
Thermophysical properties[edit]
Common PCMs[edit]
Volumetric heat capacity (VHC) J·m−3·K−1
Thermal inertia (I) = Thermal effusivity (e) J·m−2·K−1·s−1/2
Commercially available PCMs near room temperature[edit]
The above dataset is also available as an Excel spreadsheet from UCLA Engineering
Technology, development and encapsulation[edit]
The most commonly used PCMs are salt hydrates, fatty acids and esters, and various paraffins (such as octadecane). Recently also ionic liquids were investigated as novel PCMs.
As most of the organic solutions are water-free, they can be exposed to air, but all salt based PCM solutions must be encapsulated to prevent water evaporation or uptake. Both types offer certain advantages and disadvantages and if they are correctly applied some of the disadvantages becomes an advantage for certain applications.
They have been used since the late 19th century as a medium for the thermal storage applications. They have been used in such diverse applications as refrigerated transportation[77] for rail[78] and road applications[79] and their physical properties are, therefore, well known.
Unlike the ice storage system, however, the PCM systems can be used with any conventional water chiller both for a new or alternatively retrofit application. The positive temperature phase change allows centrifugal and absorption chillers as well as the conventional reciprocating and screw chiller systems or even lower ambient conditions utilizing a cooling tower or dry cooler for charging the TES system.
The temperature range offered by the PCM technology provides a new horizon for the building services and refrigeration engineers regarding medium and high temperature energy storage applications. The scope of this thermal energy application is wide ranging of solar heating, hot water, heating rejection, i.e. cooling tower and dry cooler circuitry thermal energy storage applications.
Since PCMs transform between solid–liquid in thermal cycling, encapsulation[80] naturally become the obvious storage choice.
Encapsulation of PCMs
Macro-encapsulation: Early development of macro-encapsulation with large volume containment failed due to the poor thermal conductivity of most PCMs. PCMs tend to solidify at the edges of the containers preventing effective heat transfer.
Micro-encapsulation: Micro-encapsulation on the other hand showed no such problem. It allows the PCMs to be incorporated into construction materials, such as concrete, easily and economically. Micro-encapsulated PCMs also provide a portable heat storage system. By coating a microscopic sized PCM with a protective coating, the particles can be suspended within a continuous phase such as water. This system can be considered a phase change slurry (PCS).
Molecular-encapsulation is another technology, developed by Dupont de Nemours that allows a very high concentration of PCM within a polymer compound. It allows storage capacity up to 515 kJ/m2 for a 5 mm board (103 MJ/m3). Molecular-encapsulation allows drilling and cutting through the material without any PCM leakage.
As phase change materials perform best in small containers, therefore they are usually divided in cells. The cells are shallow to reduce static head – based on the principle of shallow container geometry. The packaging material should conduct heat well; and it should be durable enough to withstand frequent changes in the storage material's volume as phase changes occur. It should also restrict the passage of water through the walls, so the materials will not dry out (or water-out, if the material is hygroscopic). Packaging must also resist leakage and corrosion. Common packaging materials showing chemical compatibility with room temperature PCMs include stainless steel, polypropylene and polyolefin.
Thermal composites[edit]
Thermal-composites is a term given to combinations of phase change materials (PCMs) and other (usually solid) structures. A simple example is a copper-mesh immersed in a paraffin-wax. The copper-mesh within parraffin-wax can be considered a composite material, dubbed a thermal-composite. Such hybrid materials are created to achieve specific overall or bulk properties.
Thermal conductivity is a common property which is targeted for maximisation by creating thermal composites. In this case the basic idea is to increase thermal conductivity by adding a highly conducting solid (such as the copper-mesh) into the relatively low conducting PCM thus increasing overall or bulk (thermal) conductivity. If the PCM is required to flow, the solid must be porous, such as a mesh.
Solid composites such as fibre-glass or kevlar-pre-preg for the aerospace industry usually refer to a fibre (the kevlar or the glass) and a matrix (the glue which solidifies to hold fibres and provide compressive strength). A thermal composite is not so clearly defined, but could similarly refer to a matrix (solid) and the PCM which is of course usually liquid and/or solid depending on conditions. They are also meant to discover minor elements in the earth.
Applications[edit]
Phase-change material being employed in the treatment of neonates with birth asphyxia[81][82]
Applications[1][83] of phase change materials include, but are not limited to:
Thermal energy storage
Conditioning of buildings, such as 'ice-storage'
Cooling of heat and electrical engines
Cooling: food, beverages, coffee, wine, milk products, green houses
Medical applications: transportation of blood, operating tables, hot-cold therapies, treatment of birth asphyxia[81]
Human body cooling under bulky clothing or costumes.
Waste heat recovery
Off-peak power utilization: Heating hot water and Cooling
Heat pump systems
Passive storage in bioclimatic building/architecture (HDPE, paraffin)
Smoothing exothermic temperature peaks in chemical reactions
Solar power plants
Spacecraft thermal systems
Thermal comfort in vehicles
Thermal protection of electronic devices
Thermal protection of food: transport, hotel trade, ice-cream, etc.
Textiles used in clothing
Computer cooling
Turbine Inlet Chilling with thermal energy storage
Telecom shelters in tropical regions. They protect the high-value equipment in the shelter by keeping the indoor air temperature below the maximum permissible by absorbing heat generated by power-hungry equipment such as a Base Station Subsystem. In case of a power failure to conventional cooling systems, PCMs minimize use of diesel generators, and this can translate into enormous savings across thousands of telecom sites in tropics.
Fire and safety issues[edit]
Some phase change materials are suspended in water, and are relatively nontoxic. Others are hydrocarbons or other flammable materials, or are toxic. As such, PCMs must be selected and applied very carefully, in accordance with fire and building codes and sound engineering practices. Because of the increased fire risk, flamespread, smoke, potential for explosion when held in containers, and liability, it may be wise not to use flammable PCMs within residential or other regularly occupied buildings. Phase change materials are also being used in thermal regulation of electronics.
See also[edit]
Heat pipe
References[edit]
^ a b Kenisarin, M; Mahkamov, K (2007). "Solar energy storage using phase change materials". Renewable and Sustainable Energy Reviews 11 (9): 1913–1965. doi:10.1016/j.rser.2006.05.005.
^ Sharma, Atul; Tyagi, V.V.; Chen, C.R.; Buddhi, D. (2009). "Review on thermal energy storage with phase change materials and applications". Renewable and Sustainable Energy Reviews 13 (2): 318–345. doi:10.1016/j.rser.2007.10.005.
^ "Heat storage systems" (PDF) by Mary Anne White, brings a list of advantages and disadvantages of Paraffin heat storage. A more complete list can be found in AccessScience website from McGraw-Hill, DOI 10.1036/1097-8542.YB020415, last modified: March 25, 2002 based on 'Latent heat storage in concrete II, Solar Energy Materials, Hawes DW, Banu D, Feldman D, 1990, 21, pp.61–80.
^ See above: 'Heat Storage Systems' (Mary Anne White), page 2
^ Pasupathy, A; Velraj, R; Seeniraj, R (2008). "Phase change material-based building architecture for thermal management in residential and commercial establishments". Renewable and Sustainable Energy Reviews 12: 39–64. doi:10.1016/j.rser.2006.05.010.
^ HyperPhysics, most from Young, Hugh D., University Physics, 7th Ed., Addison Wesley, 1992. Table 15-5. (most data should be at 293 K (20 °C; 68 °F))
^ Ice – Thermal Properties. Engineeringtoolbox.com. Retrieved on 2011-06-05.
^ AAP (April 21, 2009). "Melburnians face 60pc water cost rise - MELBURNIANS face paying up to 60 per cent more for water and sewerage under proposals announced today by the state's economic regulator.". The Australian. Retrieved 2010-02-24.
^ a b "Sodium Sulfate-Sodium Sulfate Manufacturers, Suppliers and Exporters on Alibaba.comSulphate".
^ a b Sarı, A (2002). "Thermal and heat transfer characteristics in a latent heat storage system using lauric acid". Energy Conversion and Management 43 (18): 2493–2507. doi:10.1016/S0196-8904(01)00187-X.
^ a b H. Kakuichi et al., IEA annex 10 (1999)
^ Beare-Rogers, J.; Dieffenbacher, A.; Holm, J.V. (2001). "Lexicon of lipid nutrition (IUPAC Technical Report)". Pure and Applied Chemistry 73 (4): 685–744. doi:10.1351/pac200173040685.
^ "lauric acid Q/MHD002-2006 lauric acid CN;SHN products". Alibaba.com. Retrieved 2010-02-24.
^ "Fatty Acids – Fractioned (Asia Pacific) Price Report – Chemical pricing information". ICIS Pricing. Retrieved 2010-03-10.
^ a b Nagano, K (2003). "Thermal characteristics of manganese (II) nitrate hexahydrate as a phase change material for cooling systems". Applied Thermal Engineering 23 (2): 229–241. doi:10.1016/S1359-4311(02)00161-8.
^ a b Yinping, Zhang; Yi, Jiang; Yi, Jiang (1999). "A simple method, the -history method, of determining the heat of fusion, specific heat and thermal conductivity of phase-change materials". Measurement Science and Technology 10 (3): 201–205. Bibcode:1999MeScT..10..201Y. doi:10.1088/0957-0233/10/3/015.
^ Kalapathy, Uruthira; Proctor, Andrew; Shultz, John (2002-12-10). "Silicate Thermal Insulation Material from Rice Hull Ash". Industrial & Engineering Chemistry Research 42 (1): 46–49. doi:10.1021/ie0203227.
^ Sodium Silicate (Water Glass). Sheffield-pottery.com. Retrieved on 2011-06-05.
^ Hukseflux Thermal Sensors. Hukseflux.com. Retrieved on 2011-06-05.
^ Aluminium. Goodefellow. Web.archive.org (2008-11-13). Retrieved on 2011-06-05.
^ "Aluminum Prices, London Metal Exchange (LME) Aluminum Alloy Prices, COMEX and Shanghai Aluminum Prices". 23 February 2010. Retrieved 2010-02-24.
^ Copper. Goodfellow. Web.archive.org (2008-11-16). Retrieved on 2011-06-05.
^ a b c d e "Metal Prices and News". 23 February 2010. Retrieved 2010-02-24.
^ Gold. Goodfellow. Web.archive.org (2008-11-15). Retrieved on 2011-06-05.
^ Iron. Goodfellow. Web.archive.org (2008-11-18). Retrieved on 2011-06-05.
^ "Iron Page". 7 December 2007. Retrieved 2010-02-24.
^ Lead. Goodfellow. Web.archive.org (2008-11-18). Retrieved on 2011-06-05.
^ Lithium. Goodfellow. Web.archive.org (2008-11-18). Retrieved on 2011-06-05.
^ "Historical Price Query". August 14, 2009. Retrieved 2010-02-24.
^ Silver. Goodfellow. Web.archive.org (2008-11-17). Retrieved on 2011-06-05.
^ Titanium. Goodfellow. Web.archive.org (2008-11-15). Retrieved on 2011-06-05.
^ "Titanium Page". 28 December 2007. Retrieved 2010-02-24.
^ Zinc. Goodfellow. Web.archive.org (2008-11-18). Retrieved on 2011-06-05.
^ a b c d e f g h i j k l m n o Tamme, Rainer (February 20, 2003). "Phase Change - Storage Systems" (PDF).
^ a b c d e f g h i j k l m n o p q r s t u v w x y z aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au av aw ax ay az ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bv bw bx by bz ca cb cc cd ce cf cg ch ci cj ck cl cm cn "Review on thermal energy storage with phase change materials and applications". Renewable and Sustainable Energy Reviews 13: 318–345. doi:10.1016/j.rser.2007.10.005.
^ "Pluss®".
^ "TECHNICAL DATA SHEET OF HS 33N" (PDF).
^ "Pluss®".
^ "TECHNICAL DATA SHEET – savE® HS 26N" (PDF).
^ "TECHNICAL DATA SHEET OF savE® HS23N" (PDF).
^ "TECHNICAL DATA SHEET OF savE® HS15N" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 7N" (PDF).
^ "TECHNICAL DATA SHEET – savE® Minus 7" (PDF).
^ "Pluss®".
^ "TECHNICAL DATA SHEET – savE® OM 03" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 05" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 08" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 11" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 21" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 21" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 22" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 24" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 29" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 32" (PDF).
^ "TECHNICAL DATA SHEET – savE® HS 34" (PDF).
^ "TECHNICAL DATA SHEET – savE® OM 35a" (PDF).
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc385-TDS-OM-37.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc383-TDS-OM-46.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc395-TDS-OM-48.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/downloads/TDS/Doc445%20TDS%20OM%2050.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc384-TDS-OM-53.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc418-TDS-OM-55.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc426-TDS-OM-65.pdf.  Missing or empty |title= (help)
^ (PDF) http://www.pluss.co.in/technical-datasheets/Doc307-TDS-HS-89.pdf.  Missing or empty |title= (help)
^ PureTemp. "Technology".
^ "Honeywell, Inc.".
^ "Rubitherm GmbH".
^ "Climator - Home".
^ "Mitsubishi Chemical Corporation".
^ "CRISTOPIA Energy Systems".
^ "PCM Phase Change Material Materials Manufacturers".
^ "Phase Change Materials - BASF Dispersions & Pigments".
^ "Phase Change Materials: Thermal Management Solutions".
^ "Phase Change Materials (PCM) - Macro-encapsulated PCM manufacturing & supply".
^ "Microtek Laboratories - Microencapsulation Technology".
^ Croda International Plc. "Croda Phase Change Materials :: Products - Products".
^ Frederik Tudor the Ice King on ice transport during the 19th century
^ Richard Trevithick's steam locomotive ran in 1804
^ Amédée Bollée created steam cars beginning at 1873
^ Tyagi, Vineet Veer; Buddhi, D. (2007). "PCM thermal storage in buildings: A state of art". Renewable and Sustainable Energy Reviews 11 (6): 1146–1166. doi:10.1016/j.rser.2005.10.002.
^ a b "How two low-cost, made-in-India innovations MiraCradle & Embrace Nest are helping save the lives of newborns". timesofindia-economictimes.
^ "MiraCradle - Neonate Cooler".  C1 control character in |title= at position 11 (help)
^ Omer, A (2008). "Renewable building energy systems and passive human comfort solutions". Renewable and Sustainable Energy Reviews 12 (6): 1562–1587. doi:10.1016/j.rser.2006.07.010.
Further reading[edit]
Raoux, S. (2009). "Phase Change Materials". Annual Review of Materials Research 39: 25–48. Bibcode:2009AnRMS..39...25R. doi:10.1146/annurev-matsci-082908-145405.
Retrieved from "https://en.wikipedia.org/w/index.php?title=Phase-change_material&oldid=710452025"
Categories: Building engineeringPhysical chemistrySustainable buildingHidden categories: Pages with citations lacking titlesPages with citations having bare URLsCS1 errors: invisible charactersWikipedia introduction cleanup from September 2012All pages needing cleanupArticles covered by WikiProject Wikify from September 2012All articles covered by WikiProject Wikify