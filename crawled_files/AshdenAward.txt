Ashden - Wikipedia, the free encyclopedia
Ashden
(Redirected from Ashden Award)
Ashden is a London-based charity that works in the field of sustainable energy and development. Its work includes the annual Ashden Awards, advocacy and research in the field of sustainable energy, and mentoring and practical support for award winners.
Sarah Butler-Sloss created the awards in 2001, from the Ashden Trust, one of the Sainsbury Family Charitable Trusts.[1] In 2011 the charity changed its working name to Ashden, with its full name registered with the Charity Commission being 'Ashden, Sustainable solutions, better lives'.[2]
Contents
1 About the Ashden Awards
2 The Awards ceremony
3 Support programmes
4 Ashden seminars and conferences
5 List of winners
6 See also
7 References
8 External links
About the Ashden Awards[edit]
Ashden rewards and promotes local sustainable energy solutions in parts of Europe and the developing world through its annual Ashden Awards. Awards are given to organisations and businesses that deliver local, sustainable energy schemes with social, economic and environmental benefits. Awards are provided across several different categories, including UK and international awards. Awards for sustainable travel schemes have been provided since 2012. To help the winners, each award includes a cash prize.
The Awards ceremony[edit]
The Ashden Awards ceremony is held annually at the Royal Geographical Society in London. Previous hosts include broadcasters Emma Freud,[3] Anna Ford,[4] John Humphrys[5] and Jonathan Dimbleby[6] and environmental journalist Mark Lynas.
Guest speakers in recent years have included Kandeh Yumkella, Prince Charles, Sir David King, Wangari Maathai,[7] Al Gore, David Attenborough, Hilary Benn, (the then UK Secretary of State for International Development), Dr RK Pachauri, Chair of the UN Intergovernmental Panel on Climate Change, David Cameron, (leader of the Conservative Party) and Lord May of Oxford, (former Chief Scientific Adviser to the UK Government).
After presenting the prizes at the 2007 ceremony Al Gore commented:[8]
"No one can attend an event like the Ashden Awards and fail to be inspired. We must find a path from an unsustainable present to a sustainable future.What impresses me most about these projects is they truly are becoming the change that is needed in the world.These Awards tell us how to illuminate this path to a sustainable future together. I hope that we can make it quickly."
Support programmes[edit]
Ashden provides a package of support for organisations after they win their awards. It also runs 'LESS CO2', a peer-to-peer mentoring programme for schools to learn from each other about energy saving.[9] In 2011 Ashden helped set up the Ashden India Renewable Energy Collective,[10] made up of Ashden Award winners. The Collective works to end energy poverty in India by acting as a unified voice for the sustainable energy sector.
Ashden seminars and conferences[edit]
Ashden also holds specialist seminars and conferences bringing together their award winners with practitioners, academics, and those who make or influence policy. Examples of recent seminars include one held at Imperial College[11] and another held at DFID.[12]
List of winners[edit]
Main article: List of Ashden Award winners
The Ashden Awards have been presented to 159 organisations, including NGOs, businesses, local governments and schools.
See also[edit]
Renewable energy
Energy Globe Awards
References[edit]
^ SFCT. "The Sainsbury Family Charitable Trusts". Archived from the original on 21 July 2011. Retrieved 2011-07-01.
^ Charity Commission. "Ashden, Sustainable solutions, better lives". Retrieved 2013-09-18.
^ GreenWise. "Solid wall retrofit solution wins UK Ashden award". Retrieved 2013-09-24.
^ GVEP International. "GVEP International supports the Ashden Awards Week for 2008 Winners". Archived from the original on September 7, 2008. Retrieved 2008-11-13.
^ Helps International. "ONIL Stove Receives International Recognition". Retrieved 2008-11-13.
^ Kigali Institute of Science, Technology and Management. "KIST wins international award again". Retrieved 2008-11-13. [dead link]
^ The Independent, UK (2008-06-23). "My Week In Media: Wangari Maathai". London. Retrieved 2008-11-13.
^ Developments Magazine. "Green pioneers rewarded". Archived from the original on 15 October 2008. Retrieved 2008-11-13.
^ Times Educational Supplement. "How to bring green issues to life in your classroom". Retrieved 2013-09-25.
^ Energetica India. "The Ashden India Renewable Energy Collective to combat fuel poverty". Retrieved 2013-09-25.
^ Imperial College, London. "Ashden Awards Technical Seminar". Retrieved 2008-11-13.
^ Alliance Magazine. "Interview - Sarah Butler-Sloss". Retrieved 2008-11-13.
External links[edit]
Ashden (official site)
Ashden Sustainable Solutions, Better Lives, Registered Charity no. 1104153 at the Charity Commission
Retrieved from "https://en.wikipedia.org/w/index.php?title=Ashden&oldid=710097012"
Categories: Environmental awardsSustainabilityEnergy in the United KingdomCharities based in LondonHidden categories: All articles with dead external linksArticles with dead external links from October 2010