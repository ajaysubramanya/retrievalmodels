List of environmental degrees - Wikipedia, the free encyclopedia
List of environmental degrees
This is a list of environmental degrees, including for such interdisciplinary fields as environmental science, environmental studies, environmental engineering, environmental planning, environmental policy, sustainability science and studies, etc., at both undergraduate and graduate levels.
This list is incomplete; you can help by expanding it.
Contents
1 Bachelors
1.1 Bachelor of Arts
1.2 Bachelor of Science
1.3 Other
2 Masters
2.1 Master of Arts
2.2 Master of Professional Studies
2.3 Master of Public Administration
2.4 Master of Science
2.5 Other
3 Doctoral
3.1 Doctor of Philosophy
3.2 Other
4 See also
5 References
6 External links
Bachelors[edit]
Bachelor of Arts[edit]
Bachelor of Arts in Environmental Studies
Bachelor of Arts in Environmental Science
Bachelor of Science[edit]
Bachelor of Science in Environmental and Sustainability Studies
Bachelor of Science in Environmental Engineering
Bachelor of Science in Environmental Science(s)
Bachelor of Science in Environmental Science and Policy
Bachelor of Science in Environmental Studies
Bachelor of Science in Sustainability
Other[edit]
Bachelor of Environmental Design (BEnvD)
Bachelor of Environmental Science (BEnvSc)
Bachelor of Environmental Studies (BES)
Bachelor of Resource and Environmental Planning (BREP)
Masters[edit]
Master of Arts[edit]
Master of Arts in Environmental Policy
Master of Arts in Environmental Studies
Master of Arts in Geography and Environmental Planning
Master of Arts in International Environmental Policy
Master of Arts in Sustainability Studies
Master of Arts in Urban and Environmental Policy and Planning
Master of Professional Studies[edit]
Master of Professional Studies in Environmental Science
Master of Professional Studies in Environmental Studies
Master of Public Administration[edit]
Master of Public Administration in Environmental Policy
Master of Public Administration in Environmental Science
Master of Public Administration in Environmental Science & Policy
Master of Science[edit]
Master of Science in Economics and Policy of Energy and the Environment
Master of Science in Environmental Applied Science and Management
Master of Science in Environmental Engineering
Master of Science in Environmental Planning
Master of Science in Environmental Planning and Management
Master of Science in Environmental Policy
Master of Science in Environmental Science(s)
Master of Science in Environmental Sciences and Policy
Master of Science in Environmental Sciences, Policy and Management[1]
Master of Science in Environmental Studies
Master of Science in Sustainability
Master of Science in Sustainability Science
Master of Science in Sustainability Management
Master of Science in Systems Ecology[2]
Other[edit]
Master of Environmental Management (MEM)
Master of Environmental Planning (MEP)
Master of Environmental Planning and Design (MEPD)
Master of Environmental Policy (MEP)
Master of Environmental Policy and Management (MEPM)
Master of Environmental Science (MEnvSc)
Master of Environmental Science and Management (MESM)
Master of Philosophy in Human Ecology (M.Phil)[3]
Master of Resource Management (MRM)
Master of Sustainability (MSus)
Master of Sustainable Solutions (MSuS)
Master of Urban and Environmental Planning (MUEP)
Professional Science Master of Environmental Policy and Management (PSM)
Sustainable Masters in Business Administration (MBA)
Doctoral[edit]
Doctor of Philosophy[edit]
Doctor of Philosophy in Environmental Applied Science and Management
Doctor of Philosophy in Environmental Policy
Doctor of Philosophy in Environmental Science
Doctor of Philosophy in Environmental Science and Management
Doctor of Philosophy in Environmental Studies
Other[edit]
Doctor of Environmental Science and Engineering (DESE, or D.Env.)
Professional doctorate in Environmental Science and Engineering (D.Env.)
See also[edit]
General
List of environmental studies topics
List of master's degrees
Institutions
List of environmental degree-granting institutions
List of environmental degree-granting institutions in the United States
List of environmental design degree–granting institutions
List of institutions awarding Bachelor of Environmental Studies degrees
List of institutions awarding Bachelor of Environmental Science degrees
List of sustainability programs in North America
References[edit]
^ "MESPOM," University of Manchester. Accessed: March 10, 2015.
^ "M.S. Systems Ecology," University of Montana. Accessed: March 11, 2015.
^ "Graduate Program," College of the Atlantic. Accessed: March 11, 2015.
External links[edit]
"Environmental Programs Curriculum Study," National Council on Science and the Environment (NCSE)
"Environmental Science" (definition), United States Department of Education
"Environmental Studies" (definition), United States Department of Education
"Sustainability-focused masters degree programs," Association for the Advancement of Sustainability in Higher Education (AASHE)
Retrieved from "https://en.wikipedia.org/w/index.php?title=List_of_environmental_degrees&oldid=708304994"
Categories: Academic degreesHigher education-related listsLists of environmental topicsSustainability listsHidden categories: Articles needing additional references from March 2015All articles needing additional referencesIncomplete lists from March 2015