Industrial Assessment Center - Wikipedia, the free encyclopedia
Industrial Assessment Center
There are 24 Industrial Assessment Centers in the United States. These centers are located at universities across the US, and are funded by the United States Department of Energy (DOE) to spread ideas relating to industrial energy conservation.
The centers conduct research into energy conservation techniques for industrial applications. This is accomplished by performing energy audits or assessments at manufacturers near the particular center. The IAC program has achieved of over $4.5 billion of implemented energy cost savings since its inception.[1]
Contents
1 History
2 Other Benefits
3 Participating Universities
4 References
5 External links
History[edit]
Industrial Assessment Centers (formerly called the Energy Analysis and Diagnostic Center (EADC) program) were created by the Department of Commerce in 1976 and later moved to the DOE. The IAC program is administered through the Advanced Manufacturing Office[2] under the Office of Energy Efficiency and Renewable Energy. The Centers were created to help small and medium-sized manufacturing facilities cut back on unnecessary costs from inefficient energy use, ineffective production procedures, excess waste production, and other production-related problems.[3] According to instructions from DOE, currently the centers are only required to focus on reducing wasted energy and increasing energy efficiency. While this remains the primary focus of the assessments, waste reduction and productivity improvements are still commonly recommended.
Other Benefits[edit]
In addition to providing technical support to small to mid-sized manufacturers through energy assessments, the IAC program offers several other important benefits. Apart from the routine energy audits which cover a broad scope of industrial settings and subsystems, the IACs provide technical material and workshops promoting energy efficiency.
IAC Database
Rutgers University maintains a large databases of energy efficiency projects in the industrial sector. The database contains recommendations from every audit completed by an IAC dating back to 1980. As of September 2014, the IAC program had finished 16,500 assessments and made over 125,000 recommendations.[4] This database is free and open to the public.
IAC Alumni
The IAC program helps train the next generation of energy efficiency engineers. Hundreds of students participate in the program each year,[5] and over 56% of those students pursue careers in energy or energy efficiency.[6]
Participating Universities[edit]
Map of Centers and Contact Information
References[edit]
^ Nimbalkar, Presentation to the Industrial Energy Technology Conference, May 15, 2009
^ Advanced Manufacturing Office
^ Energy.gov IAC Site
^ DOE EERE Industrial Assessment Centers Database
^ IAC Program Metrics
^ IAC Alumni Fact Sheet
External links[edit]
IAC Websites:
Lehigh University
Boise State University
Colorado State University
University of Dayton
University of Massachusetts at Amherst
University of Missouri
Oregon State University
San Francisco State University
Syracuse University
West Virginia University
San Diego State University
Oklahoma State University
Texas A&M University
Bradley University
Indiana University – Purdue University Indianapolis
Iowa State
University of Michigan
University of Wisconsin–Milwaukee
North Carolina State University
University of Alabama
University of Miami
University of Delaware
Tennessee Tech University
IAC Field Management Office:
Rutgers University: IAC Database
Rutgers Center for Advanced Energy Systems
Retrieved from "https://en.wikipedia.org/w/index.php?title=Industrial_Assessment_Center&oldid=693789783"
Categories: Energy engineeringEnergy conservation in the United StatesEnergy research institutes