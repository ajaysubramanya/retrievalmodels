List of solid waste treatment technologies - Wikipedia, the free encyclopedia
List of solid waste treatment technologies
The following page contains a list of different forms of solid waste treatment technologies and facilities employed in waste management infrastructure.
Contents
1 Waste handling facilities
2 Established waste treatment technologies
3 Alternative waste treatment technologies
4 See also
Waste handling facilities[edit]
Civic amenity site (CA Site)
Transfer station
Established waste treatment technologies[edit]
Composting
Incineration
Landfill
Recycling
Windrow composting
Alternative waste treatment technologies[edit]
In the UK these are sometimes termed advanced waste treatment technologies, even though these technologies are not necessarily more complex than the established technologies.
Anaerobic digestion
Alcohol/ethanol production
Bioconversion of biomass to mixed alcohol fuels (pilot scale)
Biodrying
Gasification
GasPlasma: Gasification followed by syngas plasma polishing (commercial test scale)
Landfarming
In-vessel composting
Mechanical biological treatment
Mechanical heat treatment
Plasma arc waste disposal (commercial demonstration scale)
Pyrolysis
Refuse-derived fuel
Sewage treatment
Tunnel composting
UASB (applied to solid wastes)
Waste autoclave
See also[edit]
Bioethanol
Biodiesel
List of waste management companies
List of waste water treatment technologies
Pollution control
Waste-to-energy
Retrieved from "https://en.wikipedia.org/w/index.php?title=List_of_solid_waste_treatment_technologies&oldid=703023016"
Categories: Anaerobic digestionThermal treatmentWaste treatment technologyWaste-related lists