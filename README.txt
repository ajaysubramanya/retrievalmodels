###############################################################################
# README-A3-RETRIVAL MODELS
# Author : Ajay Subramanya
# subramanya{dot}a{at}husky{dot}neu{dot}edu
###############################################################################

################################ PREREQUISITES ################################

1. Maven - https://maven.apache.org/install.html
   Mac: if you have a mac and homebrew installed then just `brew install maven`
   would work
2. Java
3. A serialised inverted index-using the one I build, saved in the
   invertedIndices directory.
4. A serialised hashmap of documents and the number of tokens in them, used 
   in computing the bm25 score. Again, using the one I built. Placed in 
   invertedIndices directory

################################# RUN SCRIPT ##################################

1. 'make bm25' - to consume the inverted index previous built and compute the 
   BM25 scores for the documents in them with respect to the query provided. 
   This will output a list of top 100 docs for each query. 
2. 'make lucene' - to consume the corpus and the queries and give us the top 
    100 docs for each query.

################################# CAVEATS #####################################
 
1. If you intend to edit the queries, then edit the queries file in 
  'queries/queries.txt'. NOTE : a query should be of the format - 
   <query#></t><query>
2. If you intend to use an other inverted index with this program then you need 
   to create one in the format Map<term, Map<doc, term_freq>> and serialize it
   and place it in invertedIndices. Not a great name, but please name it as 
   'hashmap.ser' . Here the inner map holds all the docs and the corrosponding 
   term frequency for a given term. Also, 'term' & 'doc' are String and 
   'term_freq' is Integer
3. You will also need to add an other .ser file. This file is needed to compute 
   the bm25 score. This also should be a Map of the format , 
   Map<doc,num_of_tokens_in_doc> . 'doc' : String , 
  'num_of_tokens_in_doc' : Integer
4. If you want to use a different corpus with lucene, place the files under the 
   directory corpus. 

############################### DELIVERABLES ###################################

1- A readme.txt file with instructions to compile and run your programs for	
both tasks
2- Your source code for both tasks (both indexing and retrieval modules for	
Task 1) - Indexer/RetrievalModels
3- A very short report describing your implementation. - report_bm25_lucene.pdf
4- Eight tables (one per query,	for each search engine)	each containing at MOST	
100 docIDs ranked by score. - 'sampleResults'
5- A brief discussion comparing the top 5 results between the two search	
engines across all four queries. - report_bm25_lucene.pdf

################################### THE END ####################################
