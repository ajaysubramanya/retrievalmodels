package neu.ir.cs6200.bm25;

/**
 * @author ajay subramanya
 * 
 * this class is used to build an inverted index using BM25 and then query 
 * it using certain queries.
 * 
 */

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import neu.ir.cs6200.utils.Utils;

public class BM25 {
	Map<String, Map<String, Integer>> invertedIndex;
	Map<String, Integer> docIdTokens;
	Map<String, Integer> queryMap;
	Map<String, Map<String, Integer>> queryIndex;
	HashSet<String> filteredDocs;
	double k1;
	double b;
	double k2;
	double avdl;
	int r;
	int R;
	int N;

	/**
	 * constructor
	 * 
	 * @param invertedIndex
	 *            an inverted index which we would be using- this is generated
	 *            from an indexer
	 * @param docIdTokens
	 *            a map of doc Id's and the number of tokens in them
	 */
	public BM25(Map<String, Map<String, Integer>> invertedIndex, Map<String, Integer> docIdTokens) {
		this.invertedIndex = invertedIndex;
		this.docIdTokens = docIdTokens;
		this.queryIndex = new HashMap<>();
		this.queryMap = new HashMap<>();
		this.filteredDocs = new HashSet<>();
		r = 0;
		R = 0;
		k1 = 1.2;
		b = 0.75;
		k2 = 100;
		N = docIdTokens.size();
		avdl = getAvdl(docIdTokens, N);
	}

	/**
	 * to get the bm25 sorted score list for the given query
	 * 
	 * @param query
	 *            the query for which we need a bm25 score list
	 * @return doc id and their scores
	 */
	public Map<String, Double> getScores(String query) {
		buildQueryMap(query);
		buildIndexForQuery(queryMap.keySet());
		buildDocsList();
		Map<String, Double> scores = new HashMap<>();
		for (String term : queryMap.keySet()) {
			compBm25(term, scores);
		}
		return Utils.sortByValue(scores);
	}

	/**
	 * 
	 * @param term
	 *            the term for which bm25 needs to be computed
	 * @param scores
	 *            a map of doc id and their bm25 scores
	 */
	private void compBm25(String term, Map<String, Double> scores) {
		int qfI = queryMap.get(term);
		double p3 = ((k2 + 1) * qfI) / (k2 + qfI);
		for (String doc : filteredDocs) {
			Map<String, Integer> docsMap = invertedIndex.get(term);
			int f = docsMap.containsKey(doc) ? docsMap.get(doc) : 0;
			int n = docsMap.size();
			double p1 = Math.log(((r + 0.5) / (R - r + 0.5)) / ((n - r + 0.5) / (N - n - R + r + 0.5)));
			double p2 = ((k1 + 1) * f) / (getK(docIdTokens.get(doc)) + f);
			double p = p1 * p2 * p3;
			scores.put(doc, scores.containsKey(doc) ? scores.get(doc) + p : p);
		}
	}

	/**
	 * building a list of all documents that contain one or more of the query
	 * term(s)
	 */
	private void buildDocsList() {
		for (String term : queryIndex.keySet()) {
			for (String doc : queryIndex.get(term).keySet()) {
				filteredDocs.add(doc);
			}
		}
	}

	/**
	 * 
	 * @param docLen
	 *            the length of the doc for which we need to get the value of k
	 * @return the value of K based on this formula : K = k1 * ((1 - b) + b *
	 *         dl/avdl)
	 */
	private double getK(int docLen) {
		double dlByAvdl = docLen / avdl;
		return k1 * ((1 - b) + b * dlByAvdl);
	}

	/**
	 * 
	 * @param query
	 *            the query the user passes in
	 * @return a map similar to the input, but filtered by query
	 */
	private void buildIndexForQuery(Set<String> terms) {
		for (String term : terms) {
			queryIndex.put(term, invertedIndex.get(term));
		}
	}

	/**
	 * we build a query map so that we can get the query term frequency - qf
	 * 
	 * @param query
	 *            the query for which we need to get scores
	 */
	private void buildQueryMap(String query) {
		String[] terms = query.split(" +");
		for (String term : terms)
			this.queryMap.put(term, this.queryMap.containsKey(term) ? this.queryMap.get(term) + 1 : 1);
	}

	/**
	 * 
	 * @param m
	 *            the map of docs and the number of tokens in them
	 * @return the average doc length
	 */
	private static double getAvdl(Map<String, Integer> m, int N) {
		double sum = 0;
		for (String doc : m.keySet())
			sum += m.get(doc);
		return sum / N;
	}

}
