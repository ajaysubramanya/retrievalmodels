package neu.ir.cs6200.lucene;

/**
 * @author ajay subramanya
 * 
 * this class is used to build an inverted index using lucene and then query 
 * it using certain queries.
 * 
 * to use this class - 
 * 
 * 1) call the constructor by passing the path where the index could be built
 * 2) with the object that is returned by (1) invoke 'indexFileOrDirectory' 
 *    with the path of the directory containing the corpus
 * 3) with the object that is returned by (1) invoke search with the query
 *    and the number of results you need. 
 */

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import neu.ir.cs6200.utils.Utils;

public class Lucene {
	private static Analyzer analyzer = new SimpleAnalyzer(Version.LUCENE_47);
	private IndexWriter writer;
	private ArrayList<File> queue = new ArrayList<File>();
	private String indexDir;

	/**
	 * Constructor
	 * 
	 * @param indexDir
	 *            the name of the folder in which the index should be created
	 * @throws java.io.IOException
	 *             when exception creating index.
	 */
	public Lucene(String indexDir) throws IOException {
		this.indexDir = indexDir;
		FSDirectory dir = FSDirectory.open(new File(indexDir));
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_47, analyzer);
		writer = new IndexWriter(dir, config);
	}

	/**
	 * 
	 * @param query
	 *            the query to search
	 * @param numRes
	 *            the number of results to return
	 * @return a map of the doc id and its corrosponding score
	 * @throws IOException
	 */
	public Map<String, Double> search(String query, int numRes) throws IOException {
		IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(this.indexDir)));
		IndexSearcher searcher = new IndexSearcher(reader);
		TopScoreDocCollector collector = TopScoreDocCollector.create(numRes, true);
		Map<String, Double> scores = new HashMap<>();
		try {
			Query q = new QueryParser(Version.LUCENE_47, "contents", analyzer).parse(query);
			searcher.search(q, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;

			System.out.println("Found " + hits.length + " hits.");
			for (int i = 0; i < hits.length; ++i) {
				int docId = hits[i].doc;
				Document d = searcher.doc(docId);

				System.out.println((i + 1) + ". " + d.get("path") + " score=" + hits[i].score);
				scores.put(getDoc(d.get("path")), (double) hits[i].score);
			}
		} catch (Exception e) {
			System.out.println("Error searching " + query + " : " + e.getMessage());
		}
		return Utils.sortByValue(scores);
	}

	/**
	 * 
	 * @param path
	 *            the entire doc path as present in the file system Ex :
	 *            Users/someone/Documents/blah_blah.txt
	 * @return the doc name Ex : blah_blah.txt
	 */
	private String getDoc(String path) {
		return path.substring(path.lastIndexOf('/') + 1);

	}

	/**
	 * Indexes a file or directory
	 * 
	 * @param fileName
	 *            the name of a text file or a folder we wish to add to the
	 *            index
	 * @throws java.io.IOException
	 *             when exception
	 */
	public void indexFileOrDirectory(String fileName) throws IOException {
		addFiles(new File(fileName));

		int originalNumDocs = writer.numDocs();
		for (File f : queue) {
			FileReader fr = null;
			try {
				Document doc = new Document();
				fr = new FileReader(f);
				doc.add(new TextField("contents", fr));
				doc.add(new StringField("path", f.getPath(), Field.Store.YES));
				doc.add(new StringField("filename", f.getName(), Field.Store.YES));

				writer.addDocument(doc);
				System.out.println("Added: " + f);
			} catch (Exception e) {
				System.out.println("Could not add: " + f);
			} finally {
				fr.close();
			}
		}

		int newNumDocs = writer.numDocs();
		System.out.println("");
		System.out.println("************************");
		System.out.println((newNumDocs - originalNumDocs) + " documents added.");
		System.out.println("************************");

		queue.clear();
	}

	/**
	 * 
	 * @param file
	 *            add files to inverted index
	 */
	private void addFiles(File file) {

		if (!file.exists()) {
			System.out.println(file + " does not exist.");
		}
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				addFiles(f);
			}
		} else {
			String filename = file.getName().toLowerCase();

			if (filename.endsWith(".htm") || filename.endsWith(".html") || filename.endsWith(".xml")
					|| filename.endsWith(".txt")) {
				queue.add(file);
			} else {
				System.out.println("Skipped " + filename);
			}
		}
	}

	/**
	 * 
	 * @param file
	 *            add file to the queue
	 */
	private void addToQ(File file) {
		String filename = file.getName().toLowerCase();
		if (filename.endsWith(".htm") || filename.endsWith(".html") || filename.endsWith(".xml")
				|| filename.endsWith(".txt")) {
			queue.add(file);
		} else {
			System.out.println("Skipped " + filename);
		}
	}

	/**
	 * Close the index.
	 * 
	 * @throws java.io.IOException
	 *             when exception closing
	 */
	public void closeIndex() throws IOException {
		writer.close();
	}
}
