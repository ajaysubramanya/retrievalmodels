package neu.ir.cs6200;

/**
 * @author ajay subramanya
 */

import java.io.IOException;
import java.util.List;

import neu.ir.cs6200.bm25.BM25;
import neu.ir.cs6200.lucene.Lucene;
import neu.ir.cs6200.utils.Utils;

public class RetrievalModels {
	public static void main(String[] args) throws IOException {
		validate(args);
		String model = args[0];
		if (model.equals("bm25")) {
			BM25 bm25 = new BM25(Utils.deserMap(args[1]), Utils.deserTokensMap(args[2]));
			List<String> queries = Utils.readFile(args[3]);

			for (String query : queries) {
				String[] q = query.split("\t");
				Utils.writeMapToFile(bm25.getScores(q[1]), "bm25_q", q[0]);
			}
		} else if (model.equals("lucene")) {
			Lucene l = new Lucene(args[1]);
			l.indexFileOrDirectory(args[2]);
			l.closeIndex();
			List<String> queries = Utils.readFile(args[3]);

			for (String query : queries) {
				String[] q = query.split("\t");
				Utils.writeMapToFile(l.search(q[1], Integer.parseInt(args[4])), "lucene_q_", q[0]);
			}
		}
	}

	/**
	 * 
	 * @param args
	 *            the arguments passed to the program which need to be validated
	 */
	private static void validate(String[] args) {
		if (args.length == 4 && args[0].equals("bm25")) return;
		if (args.length == 5 && args[0].equals("lucene")) return;
		System.out.println("<bm25> <ser:index> <ser:doc|Tokens> <queries>");
		System.out.println("<lucene> <path_to_create_index> <path_to_corpus> <queries> <number_of_results>");
		System.exit(0);
	}
}