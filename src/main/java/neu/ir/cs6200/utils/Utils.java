package neu.ir.cs6200.utils;

/**
 * utility functions
 *
 * @author ajay subramanya
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Utils {

	/**
	 * to de-serialise the inverted index
	 * 
	 * @param file
	 *            the serialised inverted index
	 * @return the map of term and the docs
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Map<String, Integer>> deserMap(String file) {
		Map<String, Map<String, Integer>> map = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (Map<String, Map<String, Integer>>) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * to de-serialise the number of tokens in each document file
	 * 
	 * @param file
	 *            number of token per document serialised file
	 * @return map of doc and number of tokens in it
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Integer> deserTokensMap(String file) {
		Map<String, Integer> map = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			map = (Map<String, Integer>) ois.readObject();
			ois.close();
			fis.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * @author http://www.mkyong.com/java/how-to-sort-a-map-in-java/
	 * @param unsortMap
	 *            an unsorted map
	 * @return a map sorted based on value
	 */
	public static Map<String, Double> sortByValue(Map<String, Double> unsortMap) {

		List<Map.Entry<String, Double>> list = new LinkedList<Map.Entry<String, Double>>(unsortMap.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
			public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});

		Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
		for (Iterator<Map.Entry<String, Double>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Double> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * 
	 * @param path
	 *            the file path
	 * @return the contents of the path line by line
	 */
	public static List<String> readFile(String path) {
		List<String> lines = new ArrayList<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(path))) {
			String line;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;
	}

	/**
	 * utility to write map of string and integer to file
	 * 
	 * @param map
	 *            the map that will be written to the file
	 * @param fileName
	 *            the name of the file
	 */
	public static void writeMapToFile(Map<String, Double> map, String fileName, String qId) {
		try {
			File file = new File(fileName + qId);
			if (!file.exists()) file.createNewFile();
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			int rank = 1;
			for (Map.Entry<String, Double> entry : map.entrySet()) {
				bw.write(qId + " Q0 " + entry.getKey() + " " + rank + " " + entry.getValue() + " ajs_retrieval_models "
						+ "\n");
				if (rank++ == 100) break;
			}
			bw.close();
			System.out.println("wrote to file " + fileName + qId);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * for debugging
	 * 
	 * @param queryIndex
	 */
	public static void printMap(Map<String, Double> map) {
		for (String doc : map.keySet()) {
			System.out.println(" [docId] " + doc + "\t\t[score] " + map.get(doc));
		}
	}

}
