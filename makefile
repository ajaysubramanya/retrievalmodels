bm25:
	-rm -rf _*
	-rm -rf segment*
	-rm -rf bm25_results
	mvn clean package
	java -jar target/retrieval_models.jar bm25 invertedIndices/hashmap.ser invertedIndices/docIdTokens.ser queries/queries.txt
	mkdir bm25_results
	mv bm25_q* bm25_results
lucene:
	-rm -rf _*
	-rm -rf segment*
	-rm -rf lucene_results
	mvn clean package
	java -jar target/retrieval_models.jar lucene . crawled_files/ queries/queries.txt 100
	mkdir lucene_results
	mv lucene_q* lucene_results
